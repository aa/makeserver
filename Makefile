# install:
# 	pip3 install -e .

mdsrc=$(shell ls *.md)
html_from_md=$(mdsrc:%.md=%.html)

all: $(html_from_md)

print-%:
	@echo '$*=$($*)'

%.html: %.md
	pandoc --toc --css style.css --standalone $< -o $@

front/output/cmeditor.js: front/src/cmeditor.js
	cd front && pnpm run build

install-statics: front/output/cmeditor.js
	cp front/output/cmeditor.js makeserver/data/htdocs/codemirror/
