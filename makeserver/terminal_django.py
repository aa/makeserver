# chat/consumers.py
import json
from channels.generic.websocket import WebsocketConsumer
from channels.generic.websocket import AsyncWebsocketConsumer
from asgiref.sync import async_to_sync, sync_to_async
import logging

try:
    from ptyprocess import PtyProcessUnicode
except ImportError:
    from winpty import PtyProcess as PtyProcessUnicode
import signal
import uuid
import os
import asyncio
import errno

from urllib.parse import parse_qs
from shlex import quote as quote_shell

from makeserver.models import Site

# from time import sleep

ENV_PREFIX = "PYXTERM_"
DEFAULT_TERM_TYPE = "xterm"


"""
Understanding pty / virtual terminals

https://ptyprocess.readthedocs.io/en/latest/index.html
https://ptyprocess.readthedocs.io/en/latest/_images/pty_vs_popen.png
http://www.linusakesson.net/programming/tty/


"""


logger = logging.getLogger(__name__)


"""

From [the channels tutorial](https://channels.readthedocs.io/en/stable/tutorial/part_2.html):

A channel layer provides the following abstractions:

* A channel is a mailbox where messages can be sent to. Each channel has a name. Anyone who has the name of a channel can send a message to the channel.

* A group is a group of related channels. A group has a name. Anyone who has the name of a group can add/remove a channel to the group by name and send a message to all channels in the group. It is not possible to enumerate what channels are in a particular group.

Every consumer instance has an automatically generated unique channel name, and so can be communicated with via a channel layer.


"""


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        logger.info("ChatConsumer: connect")
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }))

class SubprocessConsumer(AsyncWebsocketConsumer):
    def __init__(self, *args, command="ls", **kwargs):
        self.command = command # .split()
        self.ptyproc = None
        self.group_name = None
        self.cwd = None
        super().__init__(*args, **kwargs)

    async def connect(self):
        # logger.info("SubprocessConsumer: connect")
        print(f"SubprocessConsumer: command: {self.command}")
        print(f"SubprocessConsumer: cwd: {self.cwd}")
        # self.room_name = self.scope['url_route']['kwargs']['room_name']
        # self.room_group_name = 'chat_%s' % self.room_name
        # TESTING CLOSE IMMEDIATELY
        # print ("SubprocessConsumer: TESTING ONLY RETURNING NOW")
        # print ()
        # await self.close()
        # return
        # Join room group
        # await self.channel_layer.group_add(
        #     self.room_group_name,
        #     self.channel_name
        # )
        # self.cmd = "ls"
        await self.accept()
        self.proc = await asyncio.create_subprocess_shell(
            self.command,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
            cwd = self.cwd)
        self.stdout_task = asyncio.create_task(self.reader(self.proc.stdout, "stdout"))
        self.stderr_task = asyncio.create_task(self.reader(self.proc.stderr, "stderr"))
        ret = await self.proc.wait()
        print ("StreamConsumer, returncode: ", ret)
        await self.send(text_data=json.dumps(['disconnect', ret]))
        await self.close()

    async def reader (self, stream, name, size=1024):
        while True:
            # line = await stream.read(size)
            line = await stream.readline()
            if not line:
                break
            line = line.decode().rstrip() + "\r\n"
            # print ("StreamConsumer.read:", name, line)
            await self.send(text_data=json.dumps([name, line]))

    async def disconnect(self, close_code):
        # Leave room group
        # await self.channel_layer.group_discard(
        #     self.room_group_name,
        #     self.channel_name
        # )
        print("StreamConsumer: disconnect", close_code) 

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        print ("receive", text_data_json)
        # message = text_data_json['message']

        # # Send message to room group
        # await self.channel_layer.group_send(
        #     self.room_group_name,
        #     {
        #         'type': 'chat_message',
        #         'message': message
        #     }
        # )


class TerminalConsumer(AsyncWebsocketConsumer):

    def __init__(self, *args, command="bash", **kwargs):
        self.command = command.split()
        self.ptyproc = None
        self.group_name = None
        self.cwd = None
        super().__init__(*args, **kwargs)

    def make_term_env(self, height=25, width=80, winheight=0, winwidth=0, **kwargs):
        env = os.environ.copy()
        env["TERM"] = DEFAULT_TERM_TYPE
        dimensions = "%dx%d" % (width, height)
        if winwidth and winheight:
            dimensions += ";%dx%d" % (winwidth, winheight)
        env[ENV_PREFIX+"DIMENSIONS"] = dimensions
        env["COLUMNS"] = str(width)
        env["LINES"] = str(height)

        # if self.server_url:
        #     env[ENV_PREFIX+"URL"] = self.server_url
        return env


    async def connect(self):
        # require admin
        self.user = self.scope["user"]
        if not self.user.is_staff:
            logging.info("TerminalConsumer: Unauthorized user, not connecting websocket.")
            return None
        # options = self.term_settings.copy()
        # options['shell_command'] = self.shell_command
        # options.update(kwargs)
        # argv = options['shell_command']
        await self.accept()
        await self.send(text_data=json.dumps(['setup', {}]))

        env = self.make_term_env()
        
        # cwd = options.get('cwd', None)
        # return PtyWithClients(["bash"], env)
        self.eof_received = False
        self.ptyproc = PtyProcessUnicode.spawn(self.command, env=env, cwd=self.cwd)
        loop = asyncio.get_event_loop()
        trp = TerminalReaderProtocol()
        trp.consumer = self
        read_transport, read_protocol = await loop.connect_read_pipe(lambda: trp, self.ptyproc)
        write_transport, write_protocol = await loop.connect_write_pipe(TerminalWriterProtocol, self.ptyproc)
        self.write_transport = write_transport

        # TODO
        # ?! is this important and when/how should it happen
        # a channel event could be triggered ?!
        # to let this trigger a websocket message
        #
        # self.send_json_message(["setup", {}])

    # async def start_reading (self):
    #     print ("start_reading")
    #     loop = asyncio.get_running_loop()
    #     loop.add_reader(self.ptyproc.fd, self.on_pty_read, self.ptyproc.fd)

    async def disconnect(self, close_code):
        logging.info("TerminalConsumer: disconnect")
        # TODO: kill the process (if necessary)
        if self.ptyproc is not None:
            terminated = await self.terminate()
            logging.info(f"TerminalConsumer: ptyproc terminated: {terminated}")

    # Receive message from WebSocket
    async def receive(self, text_data):
        command = json.loads(text_data)
        msg_type = command[0]
        # logging.info(f"TerminalConsumer: receive {msg_type}")

        if msg_type == "stdin":
            # await self.channel_layer.group_send(
            #     self.group_name,
            #     {
            #         'type': f'terminal_stdin',
            #         'message': command[1]
            #     }
            # )
            if not self.eof_received:
                # await sync_to_async(self.write_transport.write(command[1].encode()))()
                self.write_transport.write(command[1].encode())
            #self.terminal.ptyproc.write(command[1])
        elif msg_type == "set_size":
            # TODO: Resize the pty
            pass
            #self.size = command[1:3]
            #self.terminal.resize_to_smallest()

    async def terminal_stdin (self, event):
        # logging.info("TerminalConsumer: stdin")
        # sync_to_async(self.ptyproc.write(event['message']))()
        if not self.eof_received:
            # await sync_to_async(self.write_transport.write(event['message'].encode()))()
            self.write_transport.write(event['message'].encode())

    async def terminal_set_size (self, event):
        message = event['message']
        minrows, mincols = message
        print (f"terminal_set_size: ptyproc: {self.ptyproc}")
        # self.ptyproc.getwinsize() # this is failing with a -1 fileno ?!
        rows, cols = self.ptyproc.getwinsize() # this is failing with a -1 fileno ?!
        print(f"terminal_set_size, Current size: {rows},{cols}; New size: {message[0]},{message[1]}")
        if (rows, cols) != (minrows, mincols):
            self.ptyproc.setwinsize(minrows, mincols)

        # message should be (width, height)
        # self.terminal.resize_to_smallest()
        # for a unique terminal this doesn't need to do anything ?!

    def data_received(self, text):
        # print (f"<DATA>{text}</DATA>")
        # async_to_sync(self.channel_layer.group_send(
        #     self.group_name,
        #     {
        #         'type':'process_stdout',
        #         'text': text
        #     }
        # ))
        # learned? using async_to_sync is overkill here (and it doesn't work?!)
        # not necessary to wait for this... just schedule it
        # (is there a potential issue with ordering here?)
        # asyncio.create_task(self.channel_layer.group_send(
        #     self.group_name,
        #     {
        #         'type':'process_stdout',
        #         'text': text
        #     }
        # ))
        asyncio.create_task(self.send(text_data=json.dumps(['stdout', text])))
        
    def eof(self):
        # asyncio.create_task(self.channel_layer.group_send(
        #     self.group_name,
        #     {
        #         'type':'process_eof'
        #     }
        # ))
        self.eof_received = True
        # await self.send(text_data=json.dumps(['disconnect', self.ptyproc.exitstatus]))
        self.ptyproc.wait()
        print (f"EOF: {self.ptyproc.exitstatus}")
        asyncio.create_task(self.send(text_data=json.dumps(['disconnect', self.ptyproc.exitstatus])))

    def kill(self, sig=signal.SIGTERM):
        """Send a signal to the process in the pty"""
        self.ptyproc.kill(sig)

    def killpg(self, sig=signal.SIGTERM):
        """Send a signal to the process group of the process in the pty"""
        if os.name == 'nt':
            return self.ptyproc.kill(sig)
        pgid = os.getpgid(self.ptyproc.pid)
        os.killpg(pgid, sig)

    # @gen.coroutine
    async def terminate(self, force=False):
        '''This forces a child process to terminate. It starts nicely with
        SIGHUP and SIGINT. If "force" is True then moves onto SIGKILL. This
        returns True if the child was terminated. This returns False if the
        child could not be terminated. '''
        if os.name == 'nt':
            signals = [signal.SIGINT, signal.SIGTERM]
        else:
            signals = [signal.SIGHUP, signal.SIGCONT, signal.SIGINT,
                       signal.SIGTERM]

        # loop = IOLoop.current()
        #def sleep(): return gen.sleep(self.ptyproc.delayafterterminate)
        async def sleep():
            await asyncio.sleep(self.ptyproc.delayafterterminate)

        if not self.ptyproc.isalive():
            return True
        try:
            for sig in signals:
                self.kill(sig)
                await sleep()
                if not self.ptyproc.isalive():
                    return True
            if force:
                self.kill(signal.SIGKILL)
                await sleep()
                return not self.ptyproc.isalive()
            return False
        except OSError:
            # I think there are kernel timing issues that sometimes cause
            # this to happen. I think isalive() reports True, but the
            # process is dead to the kernel.
            # Make one last attempt to see if the kernel is up to date.
            await sleep()
            return not self.ptyproc.isalive()

class SharedTerminalConsumer(AsyncWebsocketConsumer):

    def __init__(self, *args, command="bash", **kwargs):
        self.command = command.split()
        self.ptyproc = None
        self.group_name = None
        self.cwd = None
        super().__init__(*args, **kwargs)

    def make_term_env(self, height=25, width=80, winheight=0, winwidth=0, **kwargs):
        env = os.environ.copy()
        env["TERM"] = DEFAULT_TERM_TYPE
        dimensions = "%dx%d" % (width, height)
        if winwidth and winheight:
            dimensions += ";%dx%d" % (winwidth, winheight)
        env[ENV_PREFIX+"DIMENSIONS"] = dimensions
        env["COLUMNS"] = str(width)
        env["LINES"] = str(height)

        # if self.server_url:
        #     env[ENV_PREFIX+"URL"] = self.server_url
        return env


    async def connect(self):
        # require admin
        self.user = self.scope["user"]
        if not self.user.is_staff:
            logging.info("TerminalConsumer: Unauthorized user, not connecting websocket.")
            return None
        self.group_name = uuid.uuid4().hex
        logging.info(f"TerminalConsumer: connect, using group_name: {self.group_name}")

        # if it's truly unique  ... could forgo the use of group and just send messages to the channel_layer directly ?
        await self.channel_layer.group_add(
            self.group_name,
            self.channel_name
        )

        # options = self.term_settings.copy()
        # options['shell_command'] = self.shell_command
        # options.update(kwargs)
        # argv = options['shell_command']
        env = self.make_term_env()
        
        # cwd = options.get('cwd', None)
        # return PtyWithClients(["bash"], env)
        self.eof_received = False
        self.ptyproc = PtyProcessUnicode.spawn(self.command, env=env, cwd=self.cwd)
        loop = asyncio.get_event_loop()
        trp = TerminalReaderProtocol()
        trp.consumer = self
        read_transport, read_protocol = await loop.connect_read_pipe(lambda: trp, self.ptyproc)
        write_transport, write_protocol = await loop.connect_write_pipe(TerminalWriterProtocol, self.ptyproc)
        self.write_transport = write_transport

        # TODO
        # ?! is this important and when/how should it happen
        # a channel event could be triggered ?!
        # to let this trigger a websocket message
        #
        # self.send_json_message(["setup", {}])

        await self.accept()

    # async def start_reading (self):
    #     print ("start_reading")
    #     loop = asyncio.get_running_loop()
    #     loop.add_reader(self.ptyproc.fd, self.on_pty_read, self.ptyproc.fd)

    async def disconnect(self, close_code):
        logging.info("TerminalConsumer: disconnect")
        # TODO: kill the process (if necessary)
        if self.ptyproc is not None:
            terminated = await self.terminate()
            logging.info(f"TerminalConsumer: ptyproc terminated: {terminated}")
        # Leave room group
        if self.group_name:
            await self.channel_layer.group_discard(
                self.group_name,
                self.channel_name
            )

    # Receive message from WebSocket
    async def receive(self, text_data):
        command = json.loads(text_data)
        msg_type = command[0]
        # logging.info(f"TerminalConsumer: receive {msg_type}")

        if msg_type == "stdin":
            await self.channel_layer.group_send(
                self.group_name,
                {
                    'type': f'terminal_stdin',
                    'message': command[1]
                }
            )
            #self.terminal.ptyproc.write(command[1])
        elif msg_type == "set_size":
            await self.channel_layer.group_send(
                self.group_name,
                {
                    'type': f'terminal_set_size',
                    'message': command[1:3]
                }
            )
            # TODO: Resize the pty
            #self.size = command[1:3]
            #self.terminal.resize_to_smallest()

    async def terminal_stdin (self, event):
        # logging.info("TerminalConsumer: stdin")
        # sync_to_async(self.ptyproc.write(event['message']))()
        if not self.eof_received:
            # await sync_to_async(self.write_transport.write(event['message'].encode()))()
            self.write_transport.write(event['message'].encode())

    async def terminal_set_size (self, event):
        message = event['message']
        minrows, mincols = message
        print (f"terminal_set_size: ptyproc: {self.ptyproc}")
        # self.ptyproc.getwinsize() # this is failing with a -1 fileno ?!
        rows, cols = self.ptyproc.getwinsize() # this is failing with a -1 fileno ?!
        print(f"terminal_set_size, Current size: {rows},{cols}; New size: {message[0]},{message[1]}")
        if (rows, cols) != (minrows, mincols):
            self.ptyproc.setwinsize(minrows, mincols)

        # message should be (width, height)
        # self.terminal.resize_to_smallest()
        # for a unique terminal this doesn't need to do anything ?!

    def data_received(self, text):
        # print (f"<DATA>{text}</DATA>")
        # async_to_sync(self.channel_layer.group_send(
        #     self.group_name,
        #     {
        #         'type':'process_stdout',
        #         'text': text
        #     }
        # ))
        # learned? using async_to_sync is overkill here (and it doesn't work?!)
        # not necessary to wait for this... just schedule it
        # (is there a potential issue with ordering here?)
        asyncio.create_task(self.channel_layer.group_send(
            self.group_name,
            {
                'type':'process_stdout',
                'text': text
            }
        ))

    def eof(self):
        asyncio.create_task(self.channel_layer.group_send(
            self.group_name,
            {
                'type':'process_eof'
            }
        ))


    async def process_stdout (self, event):
        """ relay process "stdout"  events to websocket """
        # print ("process_stdout")
        await self.send(text_data=json.dumps(['stdout', event['text']]))

    async def process_eof (self, event):
        # logging.info("TerminalConsumer: process_eof", self.ptyproc.exitstatus) # 2021 09 17 15:42 Debugging server
        self.eof_received = True
        await sync_to_async(self.ptyproc.wait())()

        await self.send(text_data=json.dumps(['disconnect', self.ptyproc.exitstatus]))
        # close this (websocket) connection?

    # def on_pty_died(self):
    #     """Terminal closed: tell the frontend, and close the socket.
    #     Called by Manager
    #     """
    #     # also need to call this with await
    #     self.send_json_message(['disconnect', 1])
    #     # self.close()
    #     self.ws.close()
    #     self.terminal = None

    # kill, killpg, terminate from terminado

    def kill(self, sig=signal.SIGTERM):
        """Send a signal to the process in the pty"""
        self.ptyproc.kill(sig)

    def killpg(self, sig=signal.SIGTERM):
        """Send a signal to the process group of the process in the pty"""
        if os.name == 'nt':
            return self.ptyproc.kill(sig)
        pgid = os.getpgid(self.ptyproc.pid)
        os.killpg(pgid, sig)

    # @gen.coroutine
    async def terminate(self, force=False):
        '''This forces a child process to terminate. It starts nicely with
        SIGHUP and SIGINT. If "force" is True then moves onto SIGKILL. This
        returns True if the child was terminated. This returns False if the
        child could not be terminated. '''
        if os.name == 'nt':
            signals = [signal.SIGINT, signal.SIGTERM]
        else:
            signals = [signal.SIGHUP, signal.SIGCONT, signal.SIGINT,
                       signal.SIGTERM]

        # loop = IOLoop.current()
        #def sleep(): return gen.sleep(self.ptyproc.delayafterterminate)
        async def sleep():
            await asyncio.sleep(self.ptyproc.delayafterterminate)

        if not self.ptyproc.isalive():
            return True
        try:
            for sig in signals:
                self.kill(sig)
                await sleep()
                if not self.ptyproc.isalive():
                    return True
            if force:
                self.kill(signal.SIGKILL)
                await sleep()
                return not self.ptyproc.isalive()
            return False
        except OSError:
            # I think there are kernel timing issues that sometimes cause
            # this to happen. I think isalive() reports True, but the
            # process is dead to the kernel.
            # Make one last attempt to see if the kernel is up to date.
            await sleep()
            return not self.ptyproc.isalive()


class TerminalWriterProtocol (asyncio.Protocol):
    pass

class TerminalReaderProtocol (asyncio.Protocol):
    consumer = None

    def connection_made(self, transport):
        self.transport = transport

    def data_received(self, data):
        # spawn = self.expecter.spawn
        # s = spawn._decoder.decode(data)
        # spawn._log(s, 'read')
        text = data.decode('utf8')
        # print (f"<READ>{text}</READ>")
        self.consumer.data_received(text)

    def connection_lost(self, exc):
        if isinstance(exc, OSError) and exc.errno == errno.EIO:
            # We may get here without eof_received being called, e.g on Linux
            self.eof_received()
        elif exc is not None:
            self.error(exc)

    def eof_received(self):
        print ("eof")
        self.consumer.eof()
        # N.B. If this gets called, async will close the pipe (the spawn object)
        # for us
        # try:
        #     self.expecter.spawn.flag_eof = True
        #     index = self.expecter.eof()
        # except EOF as e:
        #     self.error(e)
        # else:
        #     self.found(index)


class MakeConsumer(TerminalConsumer):

    def __init__(self, *args, command="bash", **kwargs):
        self.command = command.split()
        super().__init__(*args, **kwargs)
        ## TODO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # self.cwd = "/home/murtaugh/projects/sponge/wefts"

    async def connect(self):
        try:
            url = None
            query_string = self.scope['query_string'].decode()
            print ("MakeConsumer: query_string", query_string)
            max_depth = None
            cache = ''
            ignore_last_modified = False
            cmd = "make "
            if query_string:
                query = parse_qs(query_string)
                url = query['url'][0]
                site, path = await sync_to_async(Site.url_to_site_path)(url)
                if os.path.isfile(path):
                    self.cwd = os.path.split(path)[0]
                else:
                    self.cwd = path
                # translate url to local path
                # check/confirm makeable (find makefile)
                if 'force' in query:
                     cmd += f" -B"
                path = os.path.relpath(path, self.cwd)
                cmd +=  " " + quote_shell(path)
                # if 'cache' in query:
                #     cache = query['cache']
                #     cmd += f" --cache {query['cache'][0]}"
                # if 'ignore_last_modified' in query:
                #     cmd += f" --ignore-last-modified"
            # print ("ScrapeConsumer: url", url)
            print (f"MakeConsumer: {cmd}")
            # extract url and options
            # construct self.command
            self.command = ["bash", "-c", cmd]
            await super().connect()
        except Site.InvalidURL as e:
            print ("BAD URL")
            await self.close()

class SConsConsumer(SubprocessConsumer):

    def __init__(self, *args, command="bash", **kwargs):
        self.command = command.split()
        super().__init__(*args, **kwargs)
        ## TODO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        # self.cwd = "/home/murtaugh/projects/sponge/wefts"

    async def connect(self):
        try:
            url = None
            query_string = self.scope['query_string'].decode()
            print ("SconsConsumer: query_string", query_string)
            print ("20210919-10:00")
            max_depth = None
            cache = ''
            ignore_last_modified = False
            cmd = "scons"
            cmd += " --enable-virtualenv"
            if query_string:
                query = parse_qs(query_string)
                url = query['url'][0]
                site, path = await sync_to_async(Site.url_to_site_path)(url)
                print (f"SconsConsumer: path {path}, makefile: {site.makefile}")
                cmd += " -f "+quote_shell(site.makefile)
                # 2021 09 19: Changing set cwd to site.path (root) and use relpath (from root) to file
                # this ensures that there should only be a single .sconsign.dblite in the root
                if not os.path.exists(path) and os.path.isdir(os.path.dirname(path)):
                    # path to not yet existing file in a directory
                    self.cwd = site.path # os.path.dirname(path)
                    path = os.path.relpath(path, self.cwd)
                elif os.path.isfile(path):
                    # path to a file
                    self.cwd = site.path # os.path.split(path)[0]
                    path = os.path.relpath(path, self.cwd)
                else:
                    # assume path to directory
                    # self.cwd = path
                    # path = "index.html"
                    self.cwd = site.path
                    path = os.path.relpath(os.path.join(path, "index.html"), self.cwd)

                # translate url to local path
                # check/confirm makeable (find makefile)
                # if 'force' in query:
                #      cmd += f" -B"
                cmd +=  " "+quote_shell(path)
            print (f"SconsConsumer: {cmd}")
            self.command = cmd # ["bash", "-c", cmd]
            await super().connect()
        except Site.InvalidURL as e:
            print ("BAD URL")
            await self.close()
