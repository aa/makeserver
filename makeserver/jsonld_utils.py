import json
import rdflib
from rdflib import Graph
from pyld import jsonld

"""
Two unintended consequences of json-ld mergining...
* children list singleton turns into direct dict ref...
* loss of order of breadcrumbs!

"""



def merge (*sources : list[dict]):
    """ sources: list of dictionary objects """
    # print (f"MERGING: {len(sources)} sources")
    g = Graph()
    for i, src in enumerate(sources):
        # print (f"src{i}", json.dumps(src))
        ng = rdflib.Graph().parse(data=json.dumps(src), format="json-ld")
        # print (f"ng {len(ng)} statements")
        g += ng
    return json.loads(g.serialize(format="json-ld"))

def frame_for_id (doc, id, context):
    frame = {
        "@context": context,
        "@id": id # "file:///testing/pandoc/2023/"
    }
    return jsonld.frame(doc, frame)
