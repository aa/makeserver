import json
from asgiref.sync import async_to_sync, sync_to_async
# import logging

try:
    from ptyprocess import PtyProcessUnicode
except ImportError:
    from winpty import PtyProcess as PtyProcessUnicode
import signal
import uuid
import os
import asyncio
import errno

from urllib.parse import parse_qs
from shlex import quote as quote_shell

import aiohttp
from aiohttp import web

ENV_PREFIX = "PYXTERM_"
DEFAULT_TERM_TYPE = "xterm"


"""
Understanding pty / virtual terminals

https://ptyprocess.readthedocs.io/en/latest/index.html
https://ptyprocess.readthedocs.io/en/latest/_images/pty_vs_popen.png
http://www.linusakesson.net/programming/tty/
"""

# logger = logging.getLogger(__name__)


class TerminalWriterProtocol (asyncio.Protocol):
    pass

class TerminalReaderProtocol (asyncio.Protocol):
    consumer = None

    def connection_made(self, transport):
        self.transport = transport

    def data_received(self, data):
        text = data.decode('utf8')
        self.consumer.data_received(text)

    def connection_lost(self, exc):
        if isinstance(exc, OSError) and exc.errno == errno.EIO:
            # We may get here without eof_received being called, e.g on Linux
            self.eof_received()
        elif exc is not None:
            self.error(exc)

    def eof_received(self):
        self.consumer.eof()


async def terminal_websocket_handler (request):
    return await TerminalHandler().websocket_handler(request)

class TerminalHandler:

    def __init__(self, *args, command="bash", **kwargs):
        self.command = command.split()
        self.ptyproc = None
        self.group_name = None
        self.cwd = None

    def make_term_env(self, height=25, width=80, winheight=0, winwidth=0, **kwargs):
        env = os.environ.copy()
        env["TERM"] = DEFAULT_TERM_TYPE
        dimensions = "%dx%d" % (width, height)
        if winwidth and winheight:
            dimensions += ";%dx%d" % (winwidth, winheight)
        env[ENV_PREFIX+"DIMENSIONS"] = dimensions
        env["COLUMNS"] = str(width)
        env["LINES"] = str(height)
        return env

    async def websocket_handler(self, request):
        print ("<websocket_handler>", request)
        # logger.info("HELLO FROM LOGGER.WEBSOCKER_HANDLER", request)
        self.ws_closed = False
        self.ws = web.WebSocketResponse()
        await self.ws.prepare(request)
        await self.ws.send_json(['setup', {}])

        env = self.make_term_env()
        self.eof_received = False
        self.ptyproc = PtyProcessUnicode.spawn(self.command, env=env, cwd=self.cwd)
        loop = asyncio.get_event_loop()
        trp = TerminalReaderProtocol()
        trp.consumer = self
        # https://docs.python.org/3/library/asyncio-eventloop.html#working-with-pipes
        # https://docs.python.org/3/library/asyncio-protocol.html#asyncio-protocol
        # https://stackoverflow.com/questions/65352682/python-asyncio-pythonic-way-of-waiting-until-condition-satisfied
        self.eof_flag = asyncio.Event()
        read_transport, read_protocol = await loop.connect_read_pipe(lambda: trp, self.ptyproc)
        write_transport, write_protocol = await loop.connect_write_pipe(TerminalWriterProtocol, self.ptyproc)
        self.write_transport = write_transport

        async for msg in self.ws:
            if msg.type == aiohttp.WSMsgType.TEXT:
                command = json.loads(msg.data)
                # print ("command", command)
                msg_type = command[0]
                # logging.info(f"TerminalConsumer: receive {msg_type}")

                if msg_type == "stdin":
                    if not self.eof_received:
                        # await sync_to_async(self.write_transport.write(command[1].encode()))()
                        self.write_transport.write(command[1].encode())
                elif msg_type == "set_size":
                    # TODO: Resize the pty
                    pass
                    #self.size = command[1:3]
                    #self.terminal.resize_to_smallest()
        # websocket is closed
        # could get here (1) because webpage closed, or (2) process has terminated and ws was closed
        print (f"websocket closed, eof:{self.eof_received}")
        self.ws_closed = True
        if not self.eof_received:
            print ("terminating terminal process")
            terminated = await self.terminate()
            print (f"self.terminate: {terminated}")
        print ("waiting for self.eof_flag")
        await self.eof_flag.wait()
        print ("</websocket_handler>")
        return self.ws

    # async def disconnect(self, close_code):
    #     logging.info("TerminalConsumer: disconnect")
    #     if self.ptyproc is not None:
    #         terminated = await self.terminate()
    #         logging.info(f"TerminalConsumer: ptyproc terminated: {terminated}")

    async def terminal_stdin (self, event):
        # logging.info("TerminalConsumer: stdin")
        # sync_to_async(self.ptyproc.write(event['message']))()
        if not self.eof_received:
            # await sync_to_async(self.write_transport.write(event['message'].encode()))()
            self.write_transport.write(event['message'].encode())

    async def terminal_set_size (self, event):
        message = event['message']
        minrows, mincols = message
        print (f"terminal_set_size: ptyproc: {self.ptyproc}")
        # self.ptyproc.getwinsize() # this is failing with a -1 fileno ?!
        rows, cols = self.ptyproc.getwinsize() # this is failing with a -1 fileno ?!
        print(f"terminal_set_size, Current size: {rows},{cols}; New size: {message[0]},{message[1]}")
        if (rows, cols) != (minrows, mincols):
            self.ptyproc.setwinsize(minrows, mincols)

        # message should be (width, height)
        # self.terminal.resize_to_smallest()
        # for a unique terminal this doesn't need to do anything ?!

    def data_received(self, text):
        # learned? using async_to_sync is overkill here (and it doesn't work?!) not necessary to wait for this... just schedule it
        # (is there a potential issue with ordering here?)
        asyncio.create_task(self.ws.send_json(['stdout', text]))

    async def ws_disconnect (self, exitstatus):
        if self.ws_closed:
            return
        await self.ws.send_json(['disconnect', exitstatus])
        await self.ws.close()

    def eof(self):
        self.eof_received = True
        # await self.send(text_data=json.dumps(['disconnect', self.ptyproc.exitstatus]))
        self.ptyproc.wait()
        print (f"TerminalHandler.eof: {self.ptyproc.exitstatus}")
        # asyncio.create_task(self.ws.send_json(['disconnect', self.ptyproc.exitstatus]))
        # self.eof_flag.set()
        # asyncio.create_task(self.ws.close())
        self.eof_flag.set()
        asyncio.create_task(self.ws_disconnect(self.ptyproc.exitstatus))

    def kill(self, sig=signal.SIGTERM):
        """Send a signal to the process in the pty"""
        self.ptyproc.kill(sig)

    # def killpg(self, sig=signal.SIGTERM):
    #     """Send a signal to the process group of the process in the pty"""
    #     if os.name == 'nt':
    #         return self.ptyproc.kill(sig)
    #     pgid = os.getpgid(self.ptyproc.pid)
    #     os.killpg(pgid, sig)

    async def terminate(self, force=False):
        '''This forces a child process to terminate. It starts nicely with
        SIGHUP and SIGINT. If "force" is True then moves onto SIGKILL. This
        returns True if the child was terminated. This returns False if the
        child could not be terminated. '''
        if os.name == 'nt':
            signals = [signal.SIGINT, signal.SIGTERM]
        else:
            signals = [signal.SIGHUP, signal.SIGCONT, signal.SIGINT,
                       signal.SIGTERM]

        # loop = IOLoop.current()
        #def sleep(): return gen.sleep(self.ptyproc.delayafterterminate)
        async def sleep():
            await asyncio.sleep(self.ptyproc.delayafterterminate)

        if not self.ptyproc.isalive():
            return True
        try:
            for sig in signals:
                self.kill(sig)
                await sleep()
                if not self.ptyproc.isalive():
                    return True
            if force:
                self.kill(signal.SIGKILL)
                await sleep()
                return not self.ptyproc.isalive()
            return False
        except OSError:
            # I think there are kernel timing issues that sometimes cause
            # this to happen. I think isalive() reports True, but the
            # process is dead to the kernel.
            # Make one last attempt to see if the kernel is up to date.
            await sleep()
            return not self.ptyproc.isalive()

