import sys
import os
import argparse
import base64
import pathlib
import json
from urllib.parse import urlparse, urlunparse, unquote as urlunquote, quote as urlquote
import re

import asyncio
from asyncio import create_subprocess_exec
from asyncio.subprocess import DEVNULL, STDOUT, PIPE

import aiohttp
from aiohttp import web
from jinja2 import Environment, PackageLoader, select_autoescape


from .makefile import Makefile, Sconstruct
from .makeserver import Makeserver, CWD_BY_NAME
from .binary import is_binary_file


"""
Todo: use proper http error responses for errors, see aiohttp/http_exceptions.py
"""


MAKE = {
    'UP_TO_DATE': 0,
    'NEEDS_REMAKING': 1,
    'NOT_MAKEABLE': 2
}

MAKE_BY_CODE = {code: value for value, code in MAKE.items()}

env = Environment(
    loader=PackageLoader("makeserver"),
    autoescape=select_autoescape()
)

def path_from_request (request):
    # was: rel_url.lstrip("/")
    path = urlparse(request.rel_url.raw_path).path
    baseurl = request.app['base_url']
    path = re.sub(f"^{re.escape(baseurl)}", "", path)
    return urlunquote(path)

async def route_get (request):
    # return FileResponse(filepath, chunk_size=self._chunk_size)
    # print (f"route_get, url: {request.url}, rel_url: {request.rel_url}, rel_url.query:{request.rel_url.query}")

    # path = urlunquote(urlparse(request.rel_url.raw_path.lstrip("/")).path)
    path = path_from_request(request)
    # print (f"route_get: path:{path}")

    if path == "":
        # on root...
        # allow path to come from a url param or the actual URL
        url = request.query.get("u", "/").lstrip("/")
        path = urlunquote(url)
        if path == "":
            path = "."

    # print ("GET", path, file=sys.stderr)
    # if path == '':
    #     path = '.'
    pathisdir = os.path.isdir(path)
    makeserver = request.app['makeserver']
    verbose = request.app['verbose']
    if not pathisdir:
        if "edit" in request.rel_url.query:
            binary_p = is_binary_file(path)
            if not binary_p:
                editor = env.get_template("editor.html")
                with open(path) as fin:
                    text = fin.read()
                filename = os.path.basename(path)
                filebase, ext = os.path.splitext(filename)
                ddata = {'text': text, 'path': path, 'filename': filename, 'filebase': filebase, 'ext': ext }
                ddata['baseurl'] = request.app['base_url']
                if filename.lower() == "makefile":
                    ddata['use_spaces'] = False
                else:
                    ddata['use_spaces'] = True

                return web.Response(text=editor.render(**ddata), content_type="text/html")
            else:
                return web.Response(text="Binary editor not available")
        # only use is_makeable with paths
        # scons treats directory names as all targets in a folder
        # im = await is_makeable(path, makefile=makefile, make=makecmd, verbose=verbose)
        # im = await makefile.is_makeable(path, verbose=verbose)
        im = await makeserver.is_makeable(path, verbose=verbose)
    else:
        # REDIRECT TO INCLUDE TRAILING SLASH
        if not str(request.rel_url).endswith("/"):
            raise web.HTTPFound(f"{request.url}/")
        im = MAKE['UP_TO_DATE']
    remake = 'remake' in request.rel_url.query
    # print (f"im: {im}, remake: {remake}")
    if path and (im == MAKE['NEEDS_REMAKING'] or \
            (im == MAKE['UP_TO_DATE'] and remake)):
        # code, resp = await make(path, makefile=makefile, make=makecmd, force=remake, verbose=verbose)
        # print (f"make {path}")
        # code, resp = await makeserver.make(path, force=remake, verbose=verbose)
        # if not code:

        # bad output detection (may 2024)
        premake_mtime = None
        if not pathisdir and os.path.exists(path):
            premake_mtime = os.stat(path).st_mtime

        stdout = ''
        async for resp in makeserver.make_g(path, force=remake, verbose=verbose):
            if type(resp) == str:
                await log({'type': 'stdout', 'line': resp})
                stdout += resp
        if not (resp == 0):
            # new feature may 2024: delete file if just created (it's often blank/incorrect)
            # todo: this needs testing (esp for case of preserving a file that doesn't get clobbered by make)
            if not pathisdir and os.path.exists(path):
                if premake_mtime is None or os.stat(path).st_mtime > premake_mtime:
                    # print (f"Deleting bad make {path}", file=sys.stderr)
                    os.unlink(path) 
            template = env.get_template("error.html")
            resp_text = template.render(path=path, resp=resp, stdout=stdout)
            return web.Response(text=resp_text, content_type="text/html")
    if os.path.exists(path):
        if os.path.isdir(path):
            rup = urlparse(f"{request.url}")
            request_base = urlunparse((rup.scheme, rup.netloc, '', '', '', ''))
            ddata = await makeserver.ls(path, base=request_base, baseurl=request.app['base_url']) # editor_url=request.app['editor']
            if 'children' in ddata:
                # print (f"type children: {type(ddata['children'])}")
                if type(ddata['children']) == dict:
                    # print ("making list from singleton")
                    ddata['children'] = [ddata['children']]
                # print ("ddata.children", ddata['children'])
                ddata['children'].sort(key=lambda x: x['name'].lower())
            if 'application/json' in request.headers.get("Accept") or 'json' in request.rel_url.query:
                return web.json_response(data=ddata)                
            else:
                index = env.get_template("index.html")
                ddata['baseurl'] = request.app['base_url']
                return web.Response(text=index.render(**ddata), content_type="text/html")
        else:
            return web.FileResponse(path, chunk_size=256*1024)
    else:
        raise web.HTTPNotFound()
        # return web.Response(text="404 on path {0}".format(path))

async def route_post (request, verbose=False):
    """ write posted text value to file """
    # path = urlunquote(urlparse(request.rel_url.raw_path.lstrip("/")).path)
    path = path_from_request(request)

    if path == "":
        # on root...
        # allow path to come from a url param or the actual URL
        url = request.query.get("u", "/").lstrip("/")
        path = urlunquote(url)
        if path == "":
            path = "."
    if verbose:
        print ("POST", path, file=sys.stderr)

    if request.headers.get("content-type").startswith("multipart/"):
        # assume action="upload"
        await upload_handler(request, path)
    else:
        post = await request.post()
        action=post.get("action")
        if action == "new file":
            fp = os.path.join(path, post.get("newname"))
            pathlib.Path(fp).touch()
        elif action == "new folder":
            fp = os.path.join(path, post.get("newname"))
            os.mkdir(fp)
        elif action == "delete":
            # print ("DELETE", post.get("oldname"))
            fp = os.path.join(path, post.get("oldname"))
            if os.path.isdir(fp):
                os.rmdir(fp)
            else:
                os.unlink(fp)
        elif action == "rename":
            # print ("RENAME", post.get("oldname"), post.get("newname"))
            os.rename(os.path.join(path, post.get("oldname")), os.path.join(path, post.get("newname")))

    # REDIRECT TO GET OF SAME PAGE
    raise web.HTTPFound(request.url)

async def upload_handler(request, basepath):
    reader = await request.multipart()
    # /!\ Don't forget to validate your inputs /!\
    # reader.next() will `yield` the fields of your form

    file_count = 0
    while True:
        field = await reader.next()
        if field is None:
            break
        if field.name == "file" and field.filename:
            # print (f"saving {field.filename}")
            size = 0
            with open(os.path.join(basepath, field.filename), 'wb') as f:
                while True:
                    chunk = await field.read_chunk()  # 8192 bytes by default.
                    if not chunk:
                        break
                    size += len(chunk)
                    f.write(chunk)
                file_count += 1
    
    return web.Response(text=f"{file_count} files uploaded")
    # return web.Response(text='{} sized of {} successfully stored'.format(filename, size))



###############
# log
###############

active_sockets = []

async def log (msg):
    for ws in active_sockets:
        # await ws.send_str(msg)
        await ws.send_json(msg)

async def log_websocket_handler(request):
    # print('log.websocket connection starting')
    ws = web.WebSocketResponse()
    await ws.prepare(request)
    active_sockets.append(ws)
    # print(f'log: {len(active_sockets)} connections')

    async for msg in ws:
        # print(msg)
        if msg.type == aiohttp.WSMsgType.TEXT:
            data = json.loads(msg.data)
            # print (data)
            # if msg.data == 'close':
            #     await ws.close()

    active_sockets.remove(ws)

    # print(f'log: {len(active_sockets)} connections')
    return ws


async def prefix_index_handler (request):
    htdocs = request.app['htdocs']
    return web.FileResponse(os.path.join(htdocs, "index.html"))

async def log_handler (request):
    editor = env.get_template("log.html")
    ddata = {'baseurl': request.app['base_url']}
    return web.Response(text=editor.render(**ddata), content_type="text/html")

from .terminal import terminal_websocket_handler

async def terminal_handler (request):
    # print ("terminal_handler")
    # htdocs = request.app['htdocs']
    # return web.FileResponse(os.path.join(htdocs, "terminal", "index.html"))
    template = env.get_template("terminal.html")
    ddata = {'baseurl': request.app['base_url']}
    return web.Response(text=template.render(**ddata), content_type="text/html")


def main ():
    ap = argparse.ArgumentParser("make & serve")
    # ap.add_argument("--makefile", "-f", default="Makefile")
    ap.add_argument("--scons", action="store_true", default=False)
    ap.add_argument("--host", default="localhost")
    ap.add_argument("--port", type=int, default=8000)
    ap.add_argument("--editor", default=None)
    ap.add_argument("--static", nargs=2, default=None, action="append")
    ap.add_argument("--verbose", action="store_true", default=False)
    ap.add_argument("--version", action="store_true", default=False)
    # new style options august 2023
    ap.add_argument("--no-makefile-discovery", action="store_true", default=False)
    ap.add_argument("--makefile-cwd", default="makefile")
    ap.add_argument("--default-makefile", default=None)
    ap.add_argument("--default-makefile-cwd", default="root")
    ap.add_argument("--root", default=".")
    ap.add_argument("--no-dynamic-port", action="store_true", default=False)
    ap.add_argument("--base-url", default="/")
    ap.add_argument("--terminal", default=False, action="store_true", help="Expose a web-based terminal (experimental)")
 
    args = ap.parse_args()
    makeserver = Makeserver(
        root = args.root,
        makefile_discovery = not args.no_makefile_discovery,
        makefile_cwd = CWD_BY_NAME[args.makefile_cwd],
        default_makefile = args.default_makefile,
        default_makefile_cwd = CWD_BY_NAME[args.default_makefile_cwd])
    version = '3.1.0-dev-2023-08-05'
    print (f"Makeserver {version}")
    print ("Makeserver", makeserver)
    if args.version:
        sys.exit(0)

    if sys.platform == 'win32':
        # Folowing: https://docs.python.org/3/library/asyncio-subprocess.html
        # On Windows, the default event loop is SelectorEventLoop which does not support subprocesses.
        # ProactorEventLoop should be used instead.
        loop = asyncio.ProactorEventLoop()
        asyncio.set_event_loop(loop)
    app = web.Application()
    app['makeserver'] = makeserver
    app['verbose'] = args.verbose
    app['editor'] = args.editor
    baseurl = app['base_url'] = args.base_url
    # app.router.add_route('GET', '/ws', websocket_handler)

    data = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data")
    htdocs = os.path.join(data, "htdocs")
    app['htdocs'] = htdocs
    prefixurl = "__"
    print (f"Adding static route {prefixurl}/ -> {htdocs}")
    # add /__lab__/ => index.html
    # app.router.add_route("GET", f"/{prefixurl}/", prefix_index_handler)
    app.router.add_route("GET", f"{baseurl}{prefixurl}/log/", log_handler)
    app.router.add_route("GET", f"{baseurl}{prefixurl}/log/ws", log_websocket_handler)
    if args.terminal:
        print ("adding terminal routes")
        app.router.add_route("GET", f"{baseurl}{prefixurl}/terminal/", terminal_handler)
        app.router.add_route("GET", f"{baseurl}{prefixurl}/terminal/ws", terminal_websocket_handler)
    print ("adding static route*")
    app.router.add_static(f"{baseurl}{prefixurl}", htdocs, show_index=args.verbose)

    if args.static:
        for name, path in args.static:
            print ("Adding static route {0} -> {1}".format(name, path))
            app.router.add_static(name, path)
    app.add_routes([web.get('/{make:.*}', route_get)])
    app.add_routes([web.post('/{make:.*}', route_post)])


    port = args.port

    # DYNAMIC PORT SELECTION
    # if not args.no_dynamic_port:
    #     import socket, errno
    #     while tries<max_tries:
    #         sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #         try:
    #             sock.bind((args.host, port))
    #             sock.close()
    #             break
    #         except socket.error as e:
    #             if e.errno == errno.EADDRINUSE:
    #                 print (f"port {port} in use", file=sys.stderr)
    #                 port = port + 1
    #                 tries += 1
    #             else:
    #                 raise(e)
    # if not args.no_dynamic_port:
    #     tries = 0
    #     max_tries = 10
    #     import errno
    #     while tries<max_tries:
    #         try:
    #             web.run_app(app, host=args.host, port=port)
    #             print (f"listening on http://{args.host}:{port}/")
    #             break
    #         except OSError as e:
    #             if e.errno == errno.EADDRINUSE:
    #                 print (f"port {port} in use", file=sys.stderr)
    #                 port = port + 1
    #                 tries += 1
    #             else:
    #                 raise(e)
    # else:
    print (f"listening on http://{args.host}:{port}/")
    web.run_app(app, host=args.host, port=port)

if __name__ == "__main__":
    main()
