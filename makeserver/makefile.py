import re
# import subprocess
import os
import sys
import asyncio
from asyncio import create_subprocess_exec
from asyncio.subprocess import DEVNULL, STDOUT, PIPE
from urllib.parse import urljoin, quote as urlquote
import json
from .constants import CONTEXT

"""
make --question
        returns:
        0=file is up to date,
        1=file needs remaking,
        2=file is not makeable

Support makefile discovery
Find "closest" makefile
"""


MAKE = {
    'UP_TO_DATE': 0,
    'NEEDS_REMAKING': 1,
    'NOT_MAKEABLE': 2
}

MAKE_BY_CODE = {code: value for value, code in MAKE.items()}

class Makefile (object):
    UP_TO_DATE = 0
    NEEDS_REMAKING = 1
    NOT_MAKEABLE = 2

    def __init__(self, makefile_path:str, make_path="make"):
        self.make_path = make_path
        self.makefile = makefile_path

    def is_makeable_args(self, path, cwd):
        rpath = os.path.relpath(path, cwd)
        rmake = os.path.relpath(self.makefile, cwd)
        return [self.make_path, "--question", "-f", rmake, rpath]

    async def is_makeable (self, path, cwd=".", verbose=False):
        # rpath = os.path.relpath(path, cwd)
        # rmake = os.path.relpath(self.makefile, cwd)
        args = self.is_makeable_args(path, cwd)
        p = await create_subprocess_exec(*args, cwd = cwd, stdout=DEVNULL, stderr=DEVNULL)
        # retcode 0=file is up to date, 1=file needs remaking, 2=file is not makeable
        ret = await p.wait()
        if verbose:
            print (f"{self.make_path} --question -f {self.makefile} {path} returned: {MAKE_BY_CODE[ret]}", file=sys.stderr)
        # await log(f"is_makeable {0}: {MAKE_BY_CODE[ret]}".format(path, ret))
        return ret

    def make_args (self, path, cwd, force):
        rpath = os.path.relpath(path, cwd)
        rmake = os.path.relpath(self.makefile, cwd)
        if force:
            return [self.make_path, "-f", rmake, rpath, "-B"]
        else:
            return [self.make_path, "-f", rmake, rpath]

    async def make (self, path, cwd=".", force=False, verbose=False):
        args = self.make_args(path, cwd, force)
        p = await create_subprocess_exec(*args, stdout = PIPE, stderr = STDOUT, cwd = cwd)
        resp = ""
        while True:
            line = await p.stdout.readline()
            if not line:
                break
            resp+=line.decode("utf-8")
        ret = await p.wait() == 0
        if verbose:
            print ("make {0}: {1}".format(path, ret), file=sys.stderr)
        return (ret, resp)

    async def make_g (self, path, cwd=".", force=False, verbose=False):
        """ async generator version of above, yields process output line by line and finally the return code """
        args = self.make_args(path, cwd, force)
        p = await create_subprocess_exec(*args, stdout = PIPE, stderr = STDOUT, cwd = cwd)
        resp = ""
        while True:
            line = await p.stdout.readline()
            if not line:
                break
            yield line.decode("utf-8")
        ret = await p.wait()
        yield ret

    def ls_args (self, path, cwd):
        return [self.make_path, "-f", os.path.abspath(self.makefile), "-n", "-d"]

    def ls_process_output (self, output):
        """ returns list of strings, filenames relative to cwd """
        return [m.group(1) for m in re.finditer(r"Considering target file '([^']+)'\.", output)]

    async def ls (self, path, cwd=".", base="file://"):
        """
        todo: remove phony targets
        path refers to the directory in question
        cwd is the directory where make is run, it may be the same as path, or a parent directory up to server root
        """
        # print (f"make.ls, path:{path}, cwd:{cwd}")
        # relpath is path relative to the cwd
        path_rel_to_cwd = os.path.relpath(path, cwd)
        # print (f"ls: path: {path}, cwd:{cwd}, rp: {os.path.relpath(path, cwd)}")
        makefile = os.path.abspath(self.makefile)
        # args = [self.make_path, "-f", makefile, "-n", "-d"]
        args = self.ls_args(path, cwd)
        p = await create_subprocess_exec(*args, stdout = PIPE, stderr=STDOUT, cwd=cwd)
        output = await p.stdout.read()
        output = output.decode("utf-8")

        items = self.ls_process_output(output)
        items = [x for x in items if os.path.join(path, x) != makefile]

        # print (f"make.ls, output, {len(items)} items", items[:10])
        # make filenames relative to path (so relative to the path_relative_to_cwd)
        items = [os.path.relpath(x, path_rel_to_cwd) for x in items]
        # print (f"relative to {path}")
        # print (items)
        # filter filenames that are in path
        items = [x for x in items if os.path.split(x)[0] == ""]
        # remove directories (otherwise when merged there are weird double names with and without a trailing slash)
        # (not isdir) is used instead of isfile, to permit non-existant filenames to pass
        items = [x for x in items if not os.path.isdir(os.path.join(path, x))]
        # print ("items:", items)
        items = [{'@id': urlquote(x), 'name': x, 'redlink': not os.path.exists(os.path.join(path, x))} for x in items]
        ret = {}
        ret['@context'] = CONTEXT.copy()
        ret['@context']['@base'] = urljoin(base, urlquote(path)) # 15 sep added urlquote!
        ret['@id'] = ""
        ret['children'] = items
        return ret


class Sconstruct (Makefile):
    def __init__(self, makefile_path:str, make_path="scons"):
        super().__init__(makefile_path, make_path)

    def is_makeable_args(self, path, cwd):
        ret = super().is_makeable_args(path, cwd)
        ret.append("--enable-virtualenv")
        return ret

    def make_args(self, path, cwd, force):
        ret = super().make_args(path, cwd, force)
        ret.append("--enable-virtualenv")
        return ret

    def ls_args (self, path, cwd):
        relpath = os.path.relpath(path, cwd)
        makefile = os.path.abspath(self.makefile)
        return [self.make_path, "-f", makefile, "-n", "--tree=status", relpath]

    def ls_process_output (self, output):
        items = self.parse_tree_output(output)
        return [m['filename'] for m in items]
        # return [m.group(1) for m in re.finditer(r"Considering target file '([^']+)'\.", output)]

    def parse_tree_output(self, output):
        scons_line_pat = re.compile(r"\[(?P<status>.+?)\](?P<pipes>.+?)\+\-(?P<filename>[^\t\n\r\f\v]+)")
        items = []
        for line in output.splitlines():
            m = scons_line_pat.search(line)
            if m is not None:
                d = m.groupdict()
                d['status'] = d['status'].strip().split()
                d['pipes'] = d['pipes'].strip().split()
                depth = len(d['pipes'])
                if depth == 0:
                    d['children'] = []
                    items.append(d)
                else:
                    items[-1]['children'].append(d)
            # else:
            #     print ("no match")
        return items

if __name__ == "__main__":
    import json
    import argparse

    async def main ():
        ap = argparse.ArgumentParser()
        ap.add_argument("makefile")
        ap.add_argument("--cwd", default=".")
        ap.add_argument("--scons", action="store_true", default=False)
        args = ap.parse_args()
        # m = Sconstruct("pandoc-sconstruct.py")
        if args.scons:
            m = Sconstruct(args.makefile)
        else:
            m = Makefile(args.makefile)
        d = await m.ls("makeserver")
        print (json.dumps(d, indent=2))
    
    asyncio.run(main())
