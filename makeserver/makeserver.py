from dataclasses import dataclass
from .makefile import Makefile, Sconstruct
import os
from urllib.parse import quote as urlquote, urljoin
from .binary import is_binary_file
import json # debugging

from .jsonld_utils import *

ROOT_NAME = os.path.basename(os.path.abspath(""))
from .constants import CONTEXT


def editable (path):
    _, ext = os.path.splitext(path)
    ext = ext.lower()[1:]
    # print (f"editable? ext {ext}", file=sys.stderr)
    return ((ext not in ("html", "htm")) and not os.path.isdir(path) and not is_binary_file(path))

# from typing import Optional

# CWD (Working directory) behaviour options
CWD_ROOT:int = 0  ## use the server root as cwd when making
CWD_FILE:int = 1  ## cwd to the folder of the file
CWD_MAKEFILE:int = 2 ## cwd to the directory of a discovered makefile (discovery only)

CWD_BY_NAME = {}
CWD_BY_NAME['root'] = CWD_ROOT
CWD_BY_NAME['file'] = CWD_FILE
CWD_BY_NAME['makefile'] = CWD_MAKEFILE

VALID_MAKEFILE_NAMES = ["Makefile", "makefile", "Sconstruct.py", "sconstruct.py"]



def makefile_for_path (path):
    _, filename = os.path.split(path)
    if filename.lower() == "makefile":
        return Makefile(path)
    else:
        return Sconstruct(path)

@dataclass
class Makeserver:
    root:str
    default_makefile:str | None = None
    default_makefile_cwd: CWD_ROOT | CWD_FILE = CWD_ROOT
    makefile_discovery:bool = True
    makefile_cwd:CWD_ROOT | CWD_FILE | CWD_MAKEFILE = CWD_MAKEFILE

    def sanitize (self, path):
        """ ensure path is within root, return relative path from root """
        root = os.path.abspath(self.root)
        rpath = os.path.realpath(path)
        try:
            assert rpath.startswith(root)
        except AssertionError as e:
            print (f"{path} is not within {self.root}")
            raise e
        ret = os.path.relpath(rpath, root)
        if ret == ".":
            return ""
        return ret

    def makefile_for (self, path, verbose=False):
        path = self.sanitize(path)
        fp = os.path.join(self.root, path)
        if self.makefile_discovery:
            cur_path = fp
            while True:
                if verbose:
                    print (f"Searching for a makefile: {cur_path}")
                if os.path.isdir(cur_path):
                    for name in VALID_MAKEFILE_NAMES:
                        x = os.path.join(cur_path, name)
                        if os.path.isfile(x):
                            if self.makefile_cwd == CWD_ROOT:
                                cwd = self.root
                            elif self.makefile_cwd == CWD_MAKEFILE:
                                cwd = cur_path
                            elif self.makefile_cwd == CWD_FILE:
                                if os.path.isdir(path):
                                    cwd = path
                                else:
                                    cwd, _ = os.path.split(path)
                            return makefile_for_path(x), cwd
                if cur_path == self.root:
                    break
                cur_path, _ = os.path.split(cur_path)
        if self.default_makefile:
            if self.default_makefile_cwd == CWD_ROOT:
                cwd = self.root
            elif self.default_makefile_cwd == CWD_FILE:
                if os.path.isdir(path):
                    cwd = path
                else:
                    cwd, _ = os.path.split(path)

            return makefile_for_path(self.default_makefile), cwd
        return None, None

    async def is_makeable(self, path, verbose=False):
        mf, cwd = self.makefile_for(path)
        if mf:
            return await mf.is_makeable(path, cwd=cwd)

    async def make (self, path, force=False, verbose=False):
        mf, cwd = self.makefile_for(path)
        if mf:
            return await mf.make(path, cwd=cwd, force=force)

    async def make_g (self, path, force=False, verbose=False):
        mf, cwd = self.makefile_for(path)
        if mf:
            async for resp in mf.make_g(path, cwd=cwd, force=force):
                yield resp

    async def ls (self, path, base="file://", verbose=False, baseurl='/'):
        # print (f"ls {path}")
        files = self.directory_as_json(path, base=base, baseurl=baseurl)
        mf, cwd = self.makefile_for(path)
        # print ("files")
        # print (json.dumps(files, indent=2))
        if mf:
            try:
                targets = await mf.ls(path, cwd=cwd, base=base)
                files['makefile'] = mf.makefile
                files['makefile_edit_link'] = urlquote(mf.makefile.lstrip('.'))+"?edit"
                files['makefile_discovery'] = self.makefile_discovery            
                files['makefile_cwd'] = cwd
            except Exception as e:
                # trouble with makefile, it could be scons is not installed of another error...
                # todo: give feedback
                targets = {}
            # print ("targets")
            # print (json.dumps(targets, indent=2))
        else:
            targets = {}
        m = merge(files, targets)
        # print ("MERGE1", json.dumps(m, indent=2))
        m = frame_for_id(m, urljoin(base, urlquote(path)), files['@context'])
        # print ("MERGE", json.dumps(m, indent=2))
        # merge files + targets
        return m

    def directory_as_json(self, filepath, editor_url=None, base="file://", baseurl="/"):
        """
        requires: filepath is a relative path to directory, it should not start with /

        call makefile.ls and merge results
        """
        # returns directory's index as html
        ret = {}
        ret['@id'] = ""
        ret['@context'] = CONTEXT.copy()
        ret['@context']['@base'] = urljoin(base, urlquote(filepath)) # 15 sep 23 added urlquote
        assert os.path.isdir(filepath)

        # BREADCRUMBS
        if filepath != ".":
            # ensure filepath is without leading/trailing slashes (it should naturally be so)
            filepath = filepath.strip("/")
            path = filepath.split("/")
            # ""            => []                       => []
            # one           => ["one"]                  => [{name: "root", url: "/"}]
            # one/two       => ["one", "two"]           => [{name: "root", url: "/"}, {name: "one", "url": "/one"}]
            # one/two/three => ["one", "two", "three"]  => [{name: "root", url: "/"}, {name: "one", "url": "/one"}, {name: two, url: "/one/two"}]
            bc = []
            for i in range(len(path)):
                if i == 0:
                    bc.append({'url': baseurl, 'name': ROOT_NAME})
                else:
                    bc.append({'url': baseurl+('/'.join(path[:i])), 'name': path[i-1]})
            ret['breadcrumbs'] = bc
            # print ("breadcrumbs")
            # print (json.dumps(bc, indent=2))


        index_list = []
        dir_index = os.listdir(filepath)
        for _file in sorted(dir_index):
            if filepath == ".":
                fp = _file
            else:
                fp = os.path.join(filepath, _file)
            # file_url = '/' + urlquote(fp)
            file_url = urlquote(_file)
            target = None
            if editable(fp) and editor_url:
                file_url = "{0}#{1}".format(editor_url, file_url)
                target = "editor"

            # if file is a directory, add '/' to the end of the name
            is_dir = os.path.isdir(fp)
            if is_dir:
                file_name = "{}/".format(_file)
            else:
                file_name = _file
            stat = os.stat(fp)
            index_list.append({
                '@id': file_url,
                'name': file_name,
                'editable': editable(fp),
                'binary': is_binary_file(fp),
                'size': stat.st_size,
                'mtime': stat.st_mtime,
                'atime': stat.st_atime,
                'dir': is_dir
            })
        ret['name'] = os.path.split(filepath)[1]
        if ret['name'] == ".":
            ret['name'] = ROOT_NAME
        ret['filepath'] = filepath
        # ret['rpath'] = relative_path_to_dir # should always be filepath
        ret['children'] = index_list

        return ret

        

if __name__ == "__main__":
    md = Makeserver(".", makefile_cwd = CWD_ROOT)
    # from dataclasses import asdict
    print (md)
    print (md.sanitize(""))
