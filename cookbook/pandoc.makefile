
md=$(wildcard *.md)
mp_html=$(md:%.md=%.html)

all: $(mp_html)

%.html: %.md
	pandoc --from markdown --to html --standalone $< -o $@
