#############################
# AUDIO

mp3=$(wildcard *.mp3)
mp3_meta=$(mp3:%.mp3=.index/%.mp3/metadata.json)
# mp3_play=$(mp3}:%.mp3=.index/%.mp3/play.mp3)
# mp3_all=$(mp3_meta) $(mp3_play) $(mp3_all)

.index/%.mp3/metadata.json: %.mp3 .index/%.mp3/play.mp3
	indexalist ffmpeginfo $< | \
		indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.mp3/play.mp3 \
		--output $@

.index/%.mp3/play.mp3: %.mp3
	mkdir -p .index/$*.mp3
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a mp3
	# ffmpeg-loudnorm $< --output $@

MP3=$(wildcard *.MP3)
MP3_meta=$(MP3:%.MP3=.index/%.MP3/metadata.json)
# MP3_play=$(MP3}:%.MP3=.index/%.MP3/play.mp3)
# MP3_all=$(MP3_meta) $(MP3_play) $(MP3_all)

.index/%.MP3/metadata.json: %.MP3 .index/%.MP3/play.mp3
	indexalist ffmpeginfo $< | \
		indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.MP3/play.mp3 \
		--output $@

.index/%.MP3/play.mp3: %.MP3
	mkdir -p .index/$*.MP3
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a mp3
	# ffmpeg-loudnorm $< --output $@

wav=$(wildcard *.wav)
wav_meta=$(wav:%.wav=.index/%.wav/metadata.json)
# wav_play=$(wav}:%.wav=.index/%.wav/play.mp3)
# wav_all=$(wav_meta) $(wav_play) $(wav_all)

.index/%.wav/metadata.json: %.wav .index/%.wav/play.mp3
	indexalist ffmpeginfo $< | \
		indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.wav/play.mp3 \
		--output $@

.index/%.wav/play.mp3: %.wav
	mkdir -p .index/$*.wav
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a mp3
	# ffmpeg-loudnorm $< --output $@

WAV=$(wildcard *.WAV)
WAV_meta=$(WAV:%.WAV=.index/%.WAV/metadata.json)
# WAV_play=$(WAV}:%.WAV=.index/%.WAV/play.mp3)
# WAV_all=$(WAV_meta) $(WAV_play) $(WAV_all)

.index/%.WAV/metadata.json: %.WAV .index/%.WAV/play.mp3
	indexalist ffmpeginfo $< | \
		indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.WAV/play.mp3 \
		--output $@

.index/%.WAV/play.mp3: %.WAV
	mkdir -p .index/$*.WAV
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a mp3
	# ffmpeg-loudnorm $< --output $@

ogg=$(wildcard *.ogg)
ogg_meta=$(ogg:%.ogg=.index/%.ogg/metadata.json)
# ogg_play=$(ogg}:%.ogg=.index/%.ogg/play.mp3)
# ogg_all=$(ogg_meta) $(ogg_play) $(ogg_all)

.index/%.ogg/metadata.json: %.ogg .index/%.ogg/play.mp3
	indexalist ffmpeginfo $< | \
		indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.ogg/play.mp3 \
		--output $@

.index/%.ogg/play.mp3: %.ogg
	mkdir -p .index/$*.ogg
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a mp3
	# ffmpeg-loudnorm $< --output $@

OGG=$(wildcard *.OGG)
OGG_meta=$(OGG:%.OGG=.index/%.OGG/metadata.json)
# OGG_play=$(OGG}:%.OGG=.index/%.OGG/play.mp3)
# OGG_all=$(OGG_meta) $(OGG_play) $(OGG_all)

.index/%.OGG/metadata.json: %.OGG .index/%.OGG/play.mp3
	indexalist ffmpeginfo $< | \
		indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.OGG/play.mp3 \
		--output $@

.index/%.OGG/play.mp3: %.OGG
	mkdir -p .index/$*.OGG
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a mp3
	# ffmpeg-loudnorm $< --output $@

oga=$(wildcard *.oga)
oga_meta=$(oga:%.oga=.index/%.oga/metadata.json)
# oga_play=$(oga}:%.oga=.index/%.oga/play.mp3)
# oga_all=$(oga_meta) $(oga_play) $(oga_all)

.index/%.oga/metadata.json: %.oga .index/%.oga/play.mp3
	indexalist ffmpeginfo $< | \
		indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.oga/play.mp3 \
		--output $@

.index/%.oga/play.mp3: %.oga
	mkdir -p .index/$*.oga
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a mp3
	# ffmpeg-loudnorm $< --output $@

OGA=$(wildcard *.OGA)
OGA_meta=$(OGA:%.OGA=.index/%.OGA/metadata.json)
# OGA_play=$(OGA}:%.OGA=.index/%.OGA/play.mp3)
# OGA_all=$(OGA_meta) $(OGA_play) $(OGA_all)

.index/%.OGA/metadata.json: %.OGA .index/%.OGA/play.mp3
	indexalist ffmpeginfo $< | \
		indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.OGA/play.mp3 \
		--output $@

.index/%.OGA/play.mp3: %.OGA
	mkdir -p .index/$*.OGA
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a mp3
	# ffmpeg-loudnorm $< --output $@

m4a=$(wildcard *.m4a)
m4a_meta=$(m4a:%.m4a=.index/%.m4a/metadata.json)
# m4a_play=$(m4a}:%.m4a=.index/%.m4a/play.mp3)
# m4a_all=$(m4a_meta) $(m4a_play) $(m4a_all)

.index/%.m4a/metadata.json: %.m4a .index/%.m4a/play.mp3
	indexalist ffmpeginfo $< | \
		indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.m4a/play.mp3 \
		--output $@

.index/%.m4a/play.mp3: %.m4a
	mkdir -p .index/$*.m4a
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a mp3
	# ffmpeg-loudnorm $< --output $@

M4A=$(wildcard *.M4A)
M4A_meta=$(M4A:%.M4A=.index/%.M4A/metadata.json)
# M4A_play=$(M4A}:%.M4A=.index/%.M4A/play.mp3)
# M4A_all=$(M4A_meta) $(M4A_play) $(M4A_all)

.index/%.M4A/metadata.json: %.M4A .index/%.M4A/play.mp3
	indexalist ffmpeginfo $< | \
		indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.M4A/play.mp3 \
		--output $@

.index/%.M4A/play.mp3: %.M4A
	mkdir -p .index/$*.M4A
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a mp3
	# ffmpeg-loudnorm $< --output $@

opus=$(wildcard *.opus)
opus_meta=$(opus:%.opus=.index/%.opus/metadata.json)
# opus_play=$(opus}:%.opus=.index/%.opus/play.mp3)
# opus_all=$(opus_meta) $(opus_play) $(opus_all)

.index/%.opus/metadata.json: %.opus .index/%.opus/play.mp3
	indexalist ffmpeginfo $< | \
		indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.opus/play.mp3 \
		--output $@

.index/%.opus/play.mp3: %.opus
	mkdir -p .index/$*.opus
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a mp3
	# ffmpeg-loudnorm $< --output $@

OPUS=$(wildcard *.OPUS)
OPUS_meta=$(OPUS:%.OPUS=.index/%.OPUS/metadata.json)
# OPUS_play=$(OPUS}:%.OPUS=.index/%.OPUS/play.mp3)
# OPUS_all=$(OPUS_meta) $(OPUS_play) $(OPUS_all)

.index/%.OPUS/metadata.json: %.OPUS .index/%.OPUS/play.mp3
	indexalist ffmpeginfo $< | \
		indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.OPUS/play.mp3 \
		--output $@

.index/%.OPUS/play.mp3: %.OPUS
	mkdir -p .index/$*.OPUS
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a mp3
	# ffmpeg-loudnorm $< --output $@


#############################
# PDF

pdf=$(wildcard *.pdf)
pdf_meta=$(pdf:%.pdf=.index/%.pdf/metadata.json)
# _poster=$(:%.=.index/%./poster.png)
# _thumb=$(:%.=.index/%./thumb.png)
# pdf_all=$(pdf_meta) $(pdf_poster) $(pdf_thumb)

.index/%.pdf/metadata.json: %.pdf .index/%.pdf/poster.png .index/%.pdf/thumb.png
	indexalist pdfinfo $< | \
		indexalist jsonaddkeyvalue \
			--keyvalue "id" "$$(indexalist urlencode $<)" \
			--keyvalue "poster" .index/$*.pdf/poster.png \
			--keyvalue "thumbnail" .index/$*.pdf/thumb.png \
			--output $@

.index/%.pdf/poster.png: %.pdf
	mkdir -p .index/$*.pdf
	MAGICK_TMPDIR=/home/videos/tmp convert $<[0] -background white -flatten $@

.index/%.pdf/thumb.png: .index/%.pdf/poster.png
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@



PDF=$(wildcard *.PDF)
PDF_meta=$(PDF:%.PDF=.index/%.PDF/metadata.json)
# _poster=$(:%.=.index/%./poster.png)
# _thumb=$(:%.=.index/%./thumb.png)
# PDF_all=$(PDF_meta) $(PDF_poster) $(PDF_thumb)

.index/%.PDF/metadata.json: %.PDF .index/%.PDF/poster.png .index/%.PDF/thumb.png
	indexalist pdfinfo $< | \
		indexalist jsonaddkeyvalue \
			--keyvalue "id" "$$(indexalist urlencode $<)" \
			--keyvalue "poster" .index/$*.PDF/poster.png \
			--keyvalue "thumbnail" .index/$*.PDF/thumb.png \
			--output $@

.index/%.PDF/poster.png: %.PDF
	mkdir -p .index/$*.PDF
	MAGICK_TMPDIR=/home/videos/tmp convert $<[0] -background white -flatten $@

.index/%.PDF/thumb.png: .index/%.PDF/poster.png
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@




#############################
# IMAGE

jpg=$(wildcard *.jpg)
jpg_meta=$(jpg:%.jpg=.index/%.jpg/metadata.json)

.index/%.jpg/metadata.json: %.jpg .index/%.jpg/poster.jpg .index/%.jpg/thumb.jpg
	indexalist imageinfo $< | \
		indexalist jsonaddkeyvalue \
			--keyvalue "id" "$$(indexalist urlencode $<)" \
			--keyvalue "poster" .index/$*.jpg/poster.jpg \
			--keyvalue "thumbnail" .index/$*.jpg/thumb.jpg \
			--output $@

.index/%.jpg/poster.jpg: %.jpg
	mkdir -p .index/$*.jpg
	MAGICK_TMPDIR=/home/videos/tmp convert $<[0] -auto-orient -resize 640x640 $@

.index/%.jpg/thumb.jpg: .index/%.jpg/poster.jpg
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@



JPG=$(wildcard *.JPG)
JPG_meta=$(JPG:%.JPG=.index/%.JPG/metadata.json)

.index/%.JPG/metadata.json: %.JPG .index/%.JPG/poster.jpg .index/%.JPG/thumb.jpg
	indexalist imageinfo $< | \
		indexalist jsonaddkeyvalue \
			--keyvalue "id" "$$(indexalist urlencode $<)" \
			--keyvalue "poster" .index/$*.JPG/poster.jpg \
			--keyvalue "thumbnail" .index/$*.JPG/thumb.jpg \
			--output $@

.index/%.JPG/poster.jpg: %.JPG
	mkdir -p .index/$*.JPG
	MAGICK_TMPDIR=/home/videos/tmp convert $<[0] -auto-orient -resize 640x640 $@

.index/%.JPG/thumb.jpg: .index/%.JPG/poster.jpg
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@



tiff=$(wildcard *.tiff)
tiff_meta=$(tiff:%.tiff=.index/%.tiff/metadata.json)

.index/%.tiff/metadata.json: %.tiff .index/%.tiff/poster.jpg .index/%.tiff/thumb.jpg
	indexalist imageinfo $< | \
		indexalist jsonaddkeyvalue \
			--keyvalue "id" "$$(indexalist urlencode $<)" \
			--keyvalue "poster" .index/$*.tiff/poster.jpg \
			--keyvalue "thumbnail" .index/$*.tiff/thumb.jpg \
			--output $@

.index/%.tiff/poster.jpg: %.tiff
	mkdir -p .index/$*.tiff
	MAGICK_TMPDIR=/home/videos/tmp convert $<[0] -auto-orient -resize 640x640 $@

.index/%.tiff/thumb.jpg: .index/%.tiff/poster.jpg
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@



TIFF=$(wildcard *.TIFF)
TIFF_meta=$(TIFF:%.TIFF=.index/%.TIFF/metadata.json)

.index/%.TIFF/metadata.json: %.TIFF .index/%.TIFF/poster.jpg .index/%.TIFF/thumb.jpg
	indexalist imageinfo $< | \
		indexalist jsonaddkeyvalue \
			--keyvalue "id" "$$(indexalist urlencode $<)" \
			--keyvalue "poster" .index/$*.TIFF/poster.jpg \
			--keyvalue "thumbnail" .index/$*.TIFF/thumb.jpg \
			--output $@

.index/%.TIFF/poster.jpg: %.TIFF
	mkdir -p .index/$*.TIFF
	MAGICK_TMPDIR=/home/videos/tmp convert $<[0] -auto-orient -resize 640x640 $@

.index/%.TIFF/thumb.jpg: .index/%.TIFF/poster.jpg
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@



png=$(wildcard *.png)
png_meta=$(png:%.png=.index/%.png/metadata.json)

.index/%.png/metadata.json: %.png .index/%.png/poster.jpg .index/%.png/thumb.jpg
	indexalist imageinfo $< | \
		indexalist jsonaddkeyvalue \
			--keyvalue "id" "$$(indexalist urlencode $<)" \
			--keyvalue "poster" .index/$*.png/poster.jpg \
			--keyvalue "thumbnail" .index/$*.png/thumb.jpg \
			--output $@

.index/%.png/poster.jpg: %.png
	mkdir -p .index/$*.png
	MAGICK_TMPDIR=/home/videos/tmp convert $<[0] -auto-orient -resize 640x640 $@

.index/%.png/thumb.jpg: .index/%.png/poster.jpg
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@



PNG=$(wildcard *.PNG)
PNG_meta=$(PNG:%.PNG=.index/%.PNG/metadata.json)

.index/%.PNG/metadata.json: %.PNG .index/%.PNG/poster.jpg .index/%.PNG/thumb.jpg
	indexalist imageinfo $< | \
		indexalist jsonaddkeyvalue \
			--keyvalue "id" "$$(indexalist urlencode $<)" \
			--keyvalue "poster" .index/$*.PNG/poster.jpg \
			--keyvalue "thumbnail" .index/$*.PNG/thumb.jpg \
			--output $@

.index/%.PNG/poster.jpg: %.PNG
	mkdir -p .index/$*.PNG
	MAGICK_TMPDIR=/home/videos/tmp convert $<[0] -auto-orient -resize 640x640 $@

.index/%.PNG/thumb.jpg: .index/%.PNG/poster.jpg
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@



gif=$(wildcard *.gif)
gif_meta=$(gif:%.gif=.index/%.gif/metadata.json)

.index/%.gif/metadata.json: %.gif .index/%.gif/poster.jpg .index/%.gif/thumb.jpg
	indexalist imageinfo $< | \
		indexalist jsonaddkeyvalue \
			--keyvalue "id" "$$(indexalist urlencode $<)" \
			--keyvalue "poster" .index/$*.gif/poster.jpg \
			--keyvalue "thumbnail" .index/$*.gif/thumb.jpg \
			--output $@

.index/%.gif/poster.jpg: %.gif
	mkdir -p .index/$*.gif
	MAGICK_TMPDIR=/home/videos/tmp convert $<[0] -auto-orient -resize 640x640 $@

.index/%.gif/thumb.jpg: .index/%.gif/poster.jpg
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@



GIF=$(wildcard *.GIF)
GIF_meta=$(GIF:%.GIF=.index/%.GIF/metadata.json)

.index/%.GIF/metadata.json: %.GIF .index/%.GIF/poster.jpg .index/%.GIF/thumb.jpg
	indexalist imageinfo $< | \
		indexalist jsonaddkeyvalue \
			--keyvalue "id" "$$(indexalist urlencode $<)" \
			--keyvalue "poster" .index/$*.GIF/poster.jpg \
			--keyvalue "thumbnail" .index/$*.GIF/thumb.jpg \
			--output $@

.index/%.GIF/poster.jpg: %.GIF
	mkdir -p .index/$*.GIF
	MAGICK_TMPDIR=/home/videos/tmp convert $<[0] -auto-orient -resize 640x640 $@

.index/%.GIF/thumb.jpg: .index/%.GIF/poster.jpg
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@



svg=$(wildcard *.svg)
svg_meta=$(svg:%.svg=.index/%.svg/metadata.json)

.index/%.svg/metadata.json: %.svg .index/%.svg/poster.jpg .index/%.svg/thumb.jpg
	indexalist imageinfo $< | \
		indexalist jsonaddkeyvalue \
			--keyvalue "id" "$$(indexalist urlencode $<)" \
			--keyvalue "poster" .index/$*.svg/poster.jpg \
			--keyvalue "thumbnail" .index/$*.svg/thumb.jpg \
			--output $@

.index/%.svg/poster.jpg: %.svg
	mkdir -p .index/$*.svg
	MAGICK_TMPDIR=/home/videos/tmp convert $<[0] -auto-orient -resize 640x640 $@

.index/%.svg/thumb.jpg: .index/%.svg/poster.jpg
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@



SVG=$(wildcard *.SVG)
SVG_meta=$(SVG:%.SVG=.index/%.SVG/metadata.json)

.index/%.SVG/metadata.json: %.SVG .index/%.SVG/poster.jpg .index/%.SVG/thumb.jpg
	indexalist imageinfo $< | \
		indexalist jsonaddkeyvalue \
			--keyvalue "id" "$$(indexalist urlencode $<)" \
			--keyvalue "poster" .index/$*.SVG/poster.jpg \
			--keyvalue "thumbnail" .index/$*.SVG/thumb.jpg \
			--output $@

.index/%.SVG/poster.jpg: %.SVG
	mkdir -p .index/$*.SVG
	MAGICK_TMPDIR=/home/videos/tmp convert $<[0] -auto-orient -resize 640x640 $@

.index/%.SVG/thumb.jpg: .index/%.SVG/poster.jpg
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@




#############################
# VIDEO

mp4=$(wildcard *.mp4)
mp4_meta=$(mp4:%.mp4=.index/%.mp4/metadata.json)
# mp4_poster=$(mp4:%.mp4=.index/%.mp4/poster.png)
# mp4_thumb=$(mp4:%.mp4=.index/%.mp4/thumb.png)
# mp4_play=$(mp4:%.mp4=.index/%.mp4/play.mp4)
# mp4_all=$(mp4_meta) $(mp4_play) $(mp4_poster) $(mp4_thumb)
.index/%.mp4/metadata.json: %.mp4 .index/%.mp4/play.mp4 .index/%.mp4/poster.png .index/%.mp4/thumb.png
	indexalist ffmpeginfo $< | \
	indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.mp4/play.mp4 \
		--keyvalue "thumbnail" .index/$*.mp4/thumb.png \
		--keyvalue "poster" .index/$*.mp4/poster.png \
		--output $@

.index/%.mp4/play.mp4: %.mp4
	mkdir -p .index/$*.mp4
	# ffmpeg-loudnorm-240p $< --output $@
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a aac -c:v h264 -e="-vf scale=-2:240" || \
	ffmpeg -i $< -vf scale=-2:240 $@


.index/%.mp4/poster.png: %.mp4
	mkdir -p .index/$*.mp4
	ffmpeg -i $< -ss 0 -vframes 1 $@

.index/%.mp4/thumb.png: .index/%.mp4/poster.png
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@


MP4=$(wildcard *.MP4)
MP4_meta=$(MP4:%.MP4=.index/%.MP4/metadata.json)
# MP4_poster=$(MP4:%.MP4=.index/%.MP4/poster.png)
# MP4_thumb=$(MP4:%.MP4=.index/%.MP4/thumb.png)
# MP4_play=$(MP4:%.MP4=.index/%.MP4/play.mp4)
# MP4_all=$(mp4_meta) $(mp4_play) $(mp4_poster) $(mp4_thumb)
.index/%.MP4/metadata.json: %.MP4 .index/%.MP4/play.mp4 .index/%.MP4/poster.png .index/%.MP4/thumb.png
	indexalist ffmpeginfo $< | \
	indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.MP4/play.mp4 \
		--keyvalue "thumbnail" .index/$*.MP4/thumb.png \
		--keyvalue "poster" .index/$*.MP4/poster.png \
		--output $@

.index/%.MP4/play.mp4: %.MP4
	mkdir -p .index/$*.MP4
	# ffmpeg-loudnorm-240p $< --output $@
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a aac -c:v h264 -e="-vf scale=-2:240" || \
	ffmpeg -i $< -vf scale=-2:240 $@


.index/%.MP4/poster.png: %.MP4
	mkdir -p .index/$*.MP4
	ffmpeg -i $< -ss 0 -vframes 1 $@

.index/%.MP4/thumb.png: .index/%.MP4/poster.png
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@


m4v=$(wildcard *.m4v)
m4v_meta=$(m4v:%.m4v=.index/%.m4v/metadata.json)
# m4v_poster=$(m4v:%.m4v=.index/%.m4v/poster.png)
# m4v_thumb=$(m4v:%.m4v=.index/%.m4v/thumb.png)
# m4v_play=$(m4v:%.m4v=.index/%.m4v/play.mp4)
# m4v_all=$(mp4_meta) $(mp4_play) $(mp4_poster) $(mp4_thumb)
.index/%.m4v/metadata.json: %.m4v .index/%.m4v/play.mp4 .index/%.m4v/poster.png .index/%.m4v/thumb.png
	indexalist ffmpeginfo $< | \
	indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.m4v/play.mp4 \
		--keyvalue "thumbnail" .index/$*.m4v/thumb.png \
		--keyvalue "poster" .index/$*.m4v/poster.png \
		--output $@

.index/%.m4v/play.mp4: %.m4v
	mkdir -p .index/$*.m4v
	# ffmpeg-loudnorm-240p $< --output $@
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a aac -c:v h264 -e="-vf scale=-2:240" || \
	ffmpeg -i $< -vf scale=-2:240 $@


.index/%.m4v/poster.png: %.m4v
	mkdir -p .index/$*.m4v
	ffmpeg -i $< -ss 0 -vframes 1 $@

.index/%.m4v/thumb.png: .index/%.m4v/poster.png
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@


ogv=$(wildcard *.ogv)
ogv_meta=$(ogv:%.ogv=.index/%.ogv/metadata.json)
# ogv_poster=$(ogv:%.ogv=.index/%.ogv/poster.png)
# ogv_thumb=$(ogv:%.ogv=.index/%.ogv/thumb.png)
# ogv_play=$(ogv:%.ogv=.index/%.ogv/play.mp4)
# ogv_all=$(mp4_meta) $(mp4_play) $(mp4_poster) $(mp4_thumb)
.index/%.ogv/metadata.json: %.ogv .index/%.ogv/play.mp4 .index/%.ogv/poster.png .index/%.ogv/thumb.png
	indexalist ffmpeginfo $< | \
	indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.ogv/play.mp4 \
		--keyvalue "thumbnail" .index/$*.ogv/thumb.png \
		--keyvalue "poster" .index/$*.ogv/poster.png \
		--output $@

.index/%.ogv/play.mp4: %.ogv
	mkdir -p .index/$*.ogv
	# ffmpeg-loudnorm-240p $< --output $@
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a aac -c:v h264 -e="-vf scale=-2:240" || \
	ffmpeg -i $< -vf scale=-2:240 $@


.index/%.ogv/poster.png: %.ogv
	mkdir -p .index/$*.ogv
	ffmpeg -i $< -ss 0 -vframes 1 $@

.index/%.ogv/thumb.png: .index/%.ogv/poster.png
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@


OGV=$(wildcard *.OGV)
OGV_meta=$(OGV:%.OGV=.index/%.OGV/metadata.json)
# OGV_poster=$(OGV:%.OGV=.index/%.OGV/poster.png)
# OGV_thumb=$(OGV:%.OGV=.index/%.OGV/thumb.png)
# OGV_play=$(OGV:%.OGV=.index/%.OGV/play.mp4)
# OGV_all=$(mp4_meta) $(mp4_play) $(mp4_poster) $(mp4_thumb)
.index/%.OGV/metadata.json: %.OGV .index/%.OGV/play.mp4 .index/%.OGV/poster.png .index/%.OGV/thumb.png
	indexalist ffmpeginfo $< | \
	indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.OGV/play.mp4 \
		--keyvalue "thumbnail" .index/$*.OGV/thumb.png \
		--keyvalue "poster" .index/$*.OGV/poster.png \
		--output $@

.index/%.OGV/play.mp4: %.OGV
	mkdir -p .index/$*.OGV
	# ffmpeg-loudnorm-240p $< --output $@
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a aac -c:v h264 -e="-vf scale=-2:240" || \
	ffmpeg -i $< -vf scale=-2:240 $@


.index/%.OGV/poster.png: %.OGV
	mkdir -p .index/$*.OGV
	ffmpeg -i $< -ss 0 -vframes 1 $@

.index/%.OGV/thumb.png: .index/%.OGV/poster.png
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@


mpeg=$(wildcard *.mpeg)
mpeg_meta=$(mpeg:%.mpeg=.index/%.mpeg/metadata.json)
# mpeg_poster=$(mpeg:%.mpeg=.index/%.mpeg/poster.png)
# mpeg_thumb=$(mpeg:%.mpeg=.index/%.mpeg/thumb.png)
# mpeg_play=$(mpeg:%.mpeg=.index/%.mpeg/play.mp4)
# mpeg_all=$(mp4_meta) $(mp4_play) $(mp4_poster) $(mp4_thumb)
.index/%.mpeg/metadata.json: %.mpeg .index/%.mpeg/play.mp4 .index/%.mpeg/poster.png .index/%.mpeg/thumb.png
	indexalist ffmpeginfo $< | \
	indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.mpeg/play.mp4 \
		--keyvalue "thumbnail" .index/$*.mpeg/thumb.png \
		--keyvalue "poster" .index/$*.mpeg/poster.png \
		--output $@

.index/%.mpeg/play.mp4: %.mpeg
	mkdir -p .index/$*.mpeg
	# ffmpeg-loudnorm-240p $< --output $@
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a aac -c:v h264 -e="-vf scale=-2:240" || \
	ffmpeg -i $< -vf scale=-2:240 $@


.index/%.mpeg/poster.png: %.mpeg
	mkdir -p .index/$*.mpeg
	ffmpeg -i $< -ss 0 -vframes 1 $@

.index/%.mpeg/thumb.png: .index/%.mpeg/poster.png
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@


MPEG=$(wildcard *.MPEG)
MPEG_meta=$(MPEG:%.MPEG=.index/%.MPEG/metadata.json)
# MPEG_poster=$(MPEG:%.MPEG=.index/%.MPEG/poster.png)
# MPEG_thumb=$(MPEG:%.MPEG=.index/%.MPEG/thumb.png)
# MPEG_play=$(MPEG:%.MPEG=.index/%.MPEG/play.mp4)
# MPEG_all=$(mp4_meta) $(mp4_play) $(mp4_poster) $(mp4_thumb)
.index/%.MPEG/metadata.json: %.MPEG .index/%.MPEG/play.mp4 .index/%.MPEG/poster.png .index/%.MPEG/thumb.png
	indexalist ffmpeginfo $< | \
	indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.MPEG/play.mp4 \
		--keyvalue "thumbnail" .index/$*.MPEG/thumb.png \
		--keyvalue "poster" .index/$*.MPEG/poster.png \
		--output $@

.index/%.MPEG/play.mp4: %.MPEG
	mkdir -p .index/$*.MPEG
	# ffmpeg-loudnorm-240p $< --output $@
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a aac -c:v h264 -e="-vf scale=-2:240" || \
	ffmpeg -i $< -vf scale=-2:240 $@


.index/%.MPEG/poster.png: %.MPEG
	mkdir -p .index/$*.MPEG
	ffmpeg -i $< -ss 0 -vframes 1 $@

.index/%.MPEG/thumb.png: .index/%.MPEG/poster.png
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@


webm=$(wildcard *.webm)
webm_meta=$(webm:%.webm=.index/%.webm/metadata.json)
# webm_poster=$(webm:%.webm=.index/%.webm/poster.png)
# webm_thumb=$(webm:%.webm=.index/%.webm/thumb.png)
# webm_play=$(webm:%.webm=.index/%.webm/play.mp4)
# webm_all=$(mp4_meta) $(mp4_play) $(mp4_poster) $(mp4_thumb)
.index/%.webm/metadata.json: %.webm .index/%.webm/play.mp4 .index/%.webm/poster.png .index/%.webm/thumb.png
	indexalist ffmpeginfo $< | \
	indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.webm/play.mp4 \
		--keyvalue "thumbnail" .index/$*.webm/thumb.png \
		--keyvalue "poster" .index/$*.webm/poster.png \
		--output $@

.index/%.webm/play.mp4: %.webm
	mkdir -p .index/$*.webm
	# ffmpeg-loudnorm-240p $< --output $@
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a aac -c:v h264 -e="-vf scale=-2:240" || \
	ffmpeg -i $< -vf scale=-2:240 $@


.index/%.webm/poster.png: %.webm
	mkdir -p .index/$*.webm
	ffmpeg -i $< -ss 0 -vframes 1 $@

.index/%.webm/thumb.png: .index/%.webm/poster.png
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@


WEBM=$(wildcard *.WEBM)
WEBM_meta=$(WEBM:%.WEBM=.index/%.WEBM/metadata.json)
# WEBM_poster=$(WEBM:%.WEBM=.index/%.WEBM/poster.png)
# WEBM_thumb=$(WEBM:%.WEBM=.index/%.WEBM/thumb.png)
# WEBM_play=$(WEBM:%.WEBM=.index/%.WEBM/play.mp4)
# WEBM_all=$(mp4_meta) $(mp4_play) $(mp4_poster) $(mp4_thumb)
.index/%.WEBM/metadata.json: %.WEBM .index/%.WEBM/play.mp4 .index/%.WEBM/poster.png .index/%.WEBM/thumb.png
	indexalist ffmpeginfo $< | \
	indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.WEBM/play.mp4 \
		--keyvalue "thumbnail" .index/$*.WEBM/thumb.png \
		--keyvalue "poster" .index/$*.WEBM/poster.png \
		--output $@

.index/%.WEBM/play.mp4: %.WEBM
	mkdir -p .index/$*.WEBM
	# ffmpeg-loudnorm-240p $< --output $@
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a aac -c:v h264 -e="-vf scale=-2:240" || \
	ffmpeg -i $< -vf scale=-2:240 $@


.index/%.WEBM/poster.png: %.WEBM
	mkdir -p .index/$*.WEBM
	ffmpeg -i $< -ss 0 -vframes 1 $@

.index/%.WEBM/thumb.png: .index/%.WEBM/poster.png
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@


3gp=$(wildcard *.3gp)
3gp_meta=$(3gp:%.3gp=.index/%.3gp/metadata.json)
# 3gp_poster=$(3gp:%.3gp=.index/%.3gp/poster.png)
# 3gp_thumb=$(3gp:%.3gp=.index/%.3gp/thumb.png)
# 3gp_play=$(3gp:%.3gp=.index/%.3gp/play.mp4)
# 3gp_all=$(mp4_meta) $(mp4_play) $(mp4_poster) $(mp4_thumb)
.index/%.3gp/metadata.json: %.3gp .index/%.3gp/play.mp4 .index/%.3gp/poster.png .index/%.3gp/thumb.png
	indexalist ffmpeginfo $< | \
	indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.3gp/play.mp4 \
		--keyvalue "thumbnail" .index/$*.3gp/thumb.png \
		--keyvalue "poster" .index/$*.3gp/poster.png \
		--output $@

.index/%.3gp/play.mp4: %.3gp
	mkdir -p .index/$*.3gp
	# ffmpeg-loudnorm-240p $< --output $@
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a aac -c:v h264 -e="-vf scale=-2:240" || \
	ffmpeg -i $< -vf scale=-2:240 $@


.index/%.3gp/poster.png: %.3gp
	mkdir -p .index/$*.3gp
	ffmpeg -i $< -ss 0 -vframes 1 $@

.index/%.3gp/thumb.png: .index/%.3gp/poster.png
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@


3GP=$(wildcard *.3GP)
3GP_meta=$(3GP:%.3GP=.index/%.3GP/metadata.json)
# 3GP_poster=$(3GP:%.3GP=.index/%.3GP/poster.png)
# 3GP_thumb=$(3GP:%.3GP=.index/%.3GP/thumb.png)
# 3GP_play=$(3GP:%.3GP=.index/%.3GP/play.mp4)
# 3GP_all=$(mp4_meta) $(mp4_play) $(mp4_poster) $(mp4_thumb)
.index/%.3GP/metadata.json: %.3GP .index/%.3GP/play.mp4 .index/%.3GP/poster.png .index/%.3GP/thumb.png
	indexalist ffmpeginfo $< | \
	indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.3GP/play.mp4 \
		--keyvalue "thumbnail" .index/$*.3GP/thumb.png \
		--keyvalue "poster" .index/$*.3GP/poster.png \
		--output $@

.index/%.3GP/play.mp4: %.3GP
	mkdir -p .index/$*.3GP
	# ffmpeg-loudnorm-240p $< --output $@
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a aac -c:v h264 -e="-vf scale=-2:240" || \
	ffmpeg -i $< -vf scale=-2:240 $@


.index/%.3GP/poster.png: %.3GP
	mkdir -p .index/$*.3GP
	ffmpeg -i $< -ss 0 -vframes 1 $@

.index/%.3GP/thumb.png: .index/%.3GP/poster.png
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@


mov=$(wildcard *.mov)
mov_meta=$(mov:%.mov=.index/%.mov/metadata.json)
# mov_poster=$(mov:%.mov=.index/%.mov/poster.png)
# mov_thumb=$(mov:%.mov=.index/%.mov/thumb.png)
# mov_play=$(mov:%.mov=.index/%.mov/play.mp4)
# mov_all=$(mp4_meta) $(mp4_play) $(mp4_poster) $(mp4_thumb)
.index/%.mov/metadata.json: %.mov .index/%.mov/play.mp4 .index/%.mov/poster.png .index/%.mov/thumb.png
	indexalist ffmpeginfo $< | \
	indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.mov/play.mp4 \
		--keyvalue "thumbnail" .index/$*.mov/thumb.png \
		--keyvalue "poster" .index/$*.mov/poster.png \
		--output $@

.index/%.mov/play.mp4: %.mov
	mkdir -p .index/$*.mov
	# ffmpeg-loudnorm-240p $< --output $@
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a aac -c:v h264 -e="-vf scale=-2:240" || \
	ffmpeg -i $< -vf scale=-2:240 $@


.index/%.mov/poster.png: %.mov
	mkdir -p .index/$*.mov
	ffmpeg -i $< -ss 0 -vframes 1 $@

.index/%.mov/thumb.png: .index/%.mov/poster.png
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@


MOV=$(wildcard *.MOV)
MOV_meta=$(MOV:%.MOV=.index/%.MOV/metadata.json)
# MOV_poster=$(MOV:%.MOV=.index/%.MOV/poster.png)
# MOV_thumb=$(MOV:%.MOV=.index/%.MOV/thumb.png)
# MOV_play=$(MOV:%.MOV=.index/%.MOV/play.mp4)
# MOV_all=$(mp4_meta) $(mp4_play) $(mp4_poster) $(mp4_thumb)
.index/%.MOV/metadata.json: %.MOV .index/%.MOV/play.mp4 .index/%.MOV/poster.png .index/%.MOV/thumb.png
	indexalist ffmpeginfo $< | \
	indexalist jsonaddkeyvalue \
		--keyvalue "id" "$$(indexalist urlencode $<)" \
		--keyvalue "play" .index/$*.MOV/play.mp4 \
		--keyvalue "thumbnail" .index/$*.MOV/thumb.png \
		--keyvalue "poster" .index/$*.MOV/poster.png \
		--output $@

.index/%.MOV/play.mp4: %.MOV
	mkdir -p .index/$*.MOV
	# ffmpeg-loudnorm-240p $< --output $@
	TMP=/home/videos/tmp ffmpeg-normalize $< -o $@ -t -16 -tp -1.5 -lrt 11 -c:a aac -c:v h264 -e="-vf scale=-2:240" || \
	ffmpeg -i $< -vf scale=-2:240 $@


.index/%.MOV/poster.png: %.MOV
	mkdir -p .index/$*.MOV
	ffmpeg -i $< -ss 0 -vframes 1 $@

.index/%.MOV/thumb.png: .index/%.MOV/poster.png
	MAGICK_TMPDIR=/home/videos/tmp convert $< -resize 200x200 $@


#############################

all_meta=$(jpg_meta) $(JPG_meta) $(tiff_meta) $(TIFF_meta) $(png_meta) $(PNG_meta) $(gif_meta) $(GIF_meta) $(svg_meta) $(SVG_meta) $(mp3_meta) $(MP3_meta) $(wav_meta) $(WAV_meta) $(ogg_meta) $(OGG_meta) $(oga_meta) $(OGA_meta) $(m4a_meta) $(M4A_meta) $(opus_meta) $(OPUS_meta) $(mp4_meta) $(MP4_meta) $(m4v_meta) $(ogv_meta) $(OGV_meta) $(mpeg_meta) $(MPEG_meta) $(webm_meta) $(WEBM_meta) $(3gp_meta) $(3GP_meta) $(mov_meta) $(MOV_meta) $(pdf_meta) $(PDF_meta) 

# all: index.html $(mp3_all) $(ogg_all) $(wav_all) $(pdf_all)
# index: index.html
# meta: $(all_meta)

#######################

# index 

all: index.html

# replace spaces
fixnames:
	rename "s/ /_/g" *
# special rule for debugging variables
print-%:
	@echo '$*=$($*)'

# https://www.gnu.org/software/make/manual/html_node/Special-Targets.html
# .SECONDARY with no prerequisites causes all targets to be treated as secondary (i.e., no target is removed because it is considered intermediate). 
.SECONDARY:

# .INTERMEDIATE: index.json

description.json:
	if [ ! -f description.json ]; then echo "{}" >> $@; fi

index.json: .index/index.json description.json $(all_meta)
	indexalist jsonmerge $^ | \
	indexalist jsoncountkeys --listattrib "files" --setattrib "allkeys" > $@

index.html: index.json
	indexalist template --data $< \
		/home/murtaugh/indexalist/cookbook/index-template.html \
		--output $@
