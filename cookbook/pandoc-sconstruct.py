
env = Environment()

import os
from pathlib import Path

for md in Path(".").glob("**/*.md"):
    env.Command(f"{md.with_suffix('.html')}", f"{md}", "pandoc $SOURCE -s -o $TARGET")

for x in Path(".").glob("**/*.ipynb"):
    env.Command(f"{x.with_suffix('.html')}", f"{x}", "jupyter nbconvert $SOURCE --to html")


