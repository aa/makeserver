const path = require('path');

module.exports = {
  entry: './front/cclab.js',
  mode: "development",
  output: {
    filename: 'cclab.js',
    path: path.resolve(__dirname, 'makeserver/data/htdocs/cclab'),
    publicPath: '/__lab__/cclab/', // ?
  },
  module: {
    rules: [
      { test: /\.js$/, use: ["source-map-loader"], enforce: "pre" },
      { test: /\.css$/, use: ['style-loader', 'css-loader'] },
      { test: /\.png$/, use: 'file-loader' },
    ]
  }
};