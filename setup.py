# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

try:
    long_description = open("README.rst").read()
except IOError:
    long_description = ""

setup(
    name="makeserver",
    version="3.1.0",
    description="A scaffolding webserver",
    license="MIT",
    author="Michael Murtaugh",
    packages=find_packages(),
    install_requires=['aiohttp'],
    long_description=long_description,
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.7",
    ],
    entry_points={
        'console_scripts': [
            'makeserver = makeserver:main',
        ]
    }    
)
