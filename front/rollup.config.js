import { nodeResolve } from '@rollup/plugin-node-resolve';
import terser from '@rollup/plugin-terser';

export default {
  input: 'src/cmeditor.js',
  output: {
    dir: 'output',
    format: 'iife',
    name: 'cmeditor'
  },
  plugins: [nodeResolve(), terser()]
};