Initially used **iife** as the output format, but switching to **es** modules for code splitting of editor modes. The implication however is that the editor index script needs itself to be a module then. Or should I just be building the index.js directly -- no because I add some custom behaviours (the save for instance). But still...

```javascript
import { nodeResolve } from '@rollup/plugin-node-resolve';

export default {
  input: 'src/cmeditor.js',
  output: {
    dir: 'output',
    format: 'iife',
    name: 'cmeditor'
  },
  plugins: [nodeResolve()]
};
```
