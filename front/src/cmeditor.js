// import {basicSetup, EditorView } from "codemirror"
import {EditorView, keymap} from "@codemirror/view"
import { highlightWhitespace } from "@codemirror/view"
import { EditorState, Compartment} from "@codemirror/state"
// import {indentWithTab} from "@codemirror/commands"

import {markdown} from "@codemirror/lang-markdown"
import {python} from "@codemirror/lang-python"
import {javascript} from "@codemirror/lang-javascript"
import {css} from "@codemirror/lang-css"
import {json} from "@codemirror/lang-json"
import {xml} from "@codemirror/lang-xml"
import {html} from "@codemirror/lang-html"

// basicSetup from https://github.com/codemirror/basic-setup/blob/main/src/codemirror.ts
import {highlightSpecialChars, drawSelection, highlightActiveLine, dropCursor,
  rectangularSelection, crosshairCursor,
  lineNumbers, highlightActiveLineGutter} from "@codemirror/view"
// import {Extension } from "@codemirror/state"
import {defaultHighlightStyle, syntaxHighlighting, indentOnInput, bracketMatching,
  foldGutter, foldKeymap, indentUnit } from "@codemirror/language"
import {defaultKeymap, history, historyKeymap, indentWithTab } from "@codemirror/commands"
import {searchKeymap, highlightSelectionMatches} from "@codemirror/search"
import {autocompletion, completionKeymap, closeBrackets, closeBracketsKeymap} from "@codemirror/autocomplete"
import {lintKeymap} from "@codemirror/lint"

// Useful documentation for dynamic config:
// https://codemirror.net/examples/config/
// https://codemirror.net/examples/gutter/

const basicSetup = (() => [
  highlightActiveLineGutter(),
  highlightSpecialChars(),
  history(),
  foldGutter(),
  drawSelection(),
  dropCursor(),
  EditorState.allowMultipleSelections.of(true),
  indentOnInput(),
  syntaxHighlighting(defaultHighlightStyle, {fallback: true}),
  bracketMatching(),
  closeBrackets(),
  autocompletion(),
  rectangularSelection(),
  crosshairCursor(),
  highlightActiveLine(),
  highlightSelectionMatches(),
  keymap.of([
    ...closeBracketsKeymap,
    ...defaultKeymap,
    ...searchKeymap,
    ...historyKeymap,
    ...foldKeymap,
    ...completionKeymap,
    ...lintKeymap,
    indentWithTab,
  ])
])()

let MODES = {
  'markdown': markdown,
  'html': html,
  'xml': xml,
  'json': json,
  'css': css,
  'javascript': javascript,
  'python': python
};

export function init (editor_div, wrap = false, mode="markdown", showwhitespace=false, tabSize=8, use_spaces=true, line_numbers=true) {
  let ret = {};
  let language = new Compartment;
  let tabSize_facet = new Compartment;
  let lineWrap = new Compartment;
  let showwhitespace_facet = new Compartment;
  let indentUnit_facet = new Compartment;
  let lineNumbers_facet = new Compartment;

  const fixedHeightEditor = EditorView.theme({
    "&": {'max-height': "100%"},
    ".cm-scroller": {overflow: "auto"}
  });
  let active_mode = MODES[mode.toLowerCase()];
  
  function setMode (mode_value) {
    active_mode = MODES[mode_value.toLowerCase()];
    view.dispatch({
      effects: language.reconfigure(active_mode ? active_mode() : [])
    })
  }
  ret.setMode = setMode;

  function setLineWrap(value) {
    // console.log("setLineWrap", value);
    wrap = value;
    view.dispatch({
      effects: lineWrap.reconfigure(value ? EditorView.lineWrapping : [])
    })
  }
  ret.setLineWrap = setLineWrap;

  function setShowWhitespace (value) {
    showwhitespace = value;
    view.dispatch ({
      effects: showwhitespace_facet.reconfigure(value ? highlightWhitespace() : [])
    })
  }
  ret.setShowWhitespace = setShowWhitespace;

  function setTabSize (value) {
    tabSize = value;
    console.log(`setTabSize: ${value}`);
    view.dispatch ({
      effects: tabSize_facet.reconfigure(EditorState.tabSize.of(tabSize))
    })
  }
  ret.setTabSize = setTabSize;

  function setShowWhitespace (value) {
    showwhitespace = value;
    view.dispatch ({
      effects: showwhitespace_facet.reconfigure(value ? highlightWhitespace() : [])
    })
  }
  ret.setShowWhitespace = setShowWhitespace;

  function setLineNumbers (value) {
    line_numbers = value;
    view.dispatch ({
      effects: lineNumbers_facet.reconfigure(value ? lineNumbers() : [])
    })
  }
  ret.setLineNumbers = setLineNumbers;

  function setUseSpaces (value) {
    use_spaces = value;
    view.dispatch ({
      effects: indentUnit_facet.reconfigure(use_spaces ? indentUnit.of(" ".repeat(tabSize)) : indentUnit.of("\t"))
    })
  }
  ret.setUseSpaces = setUseSpaces;

  function getText () {
    return view.state.doc.toString();
  }
  ret.getText = getText;

  let state = EditorState.create({
    doc: editor_div.textContent,
    extensions: [
      basicSetup,
      lineNumbers_facet.of(line_numbers ? lineNumbers() : []),
      indentUnit_facet.of(use_spaces ? indentUnit.of(" ".repeat(tabSize)) : indentUnit.of("\t")),
      showwhitespace_facet.of(showwhitespace ? highlightWhitespace() : []),
      language.of(active_mode ? active_mode() : []),
      tabSize_facet.of(EditorState.tabSize.of(tabSize)),
      fixedHeightEditor,
      lineWrap.of(wrap ? EditorView.lineWrapping : [])
    ]
  })
  editor_div.innerHTML = "";
  let view = new EditorView({
    state,
    parent: editor_div
  })

  return ret;
}

